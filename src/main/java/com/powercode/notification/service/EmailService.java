package com.powercode.notification.service;

import com.powercode.component.commons.client.ClientResponse;

/**
 * 邮件的业务逻辑接口层
 * 
 */
public interface EmailService {

	/**
	 * 发送邮件
	 * 
	 * @param toEmail 接收者的邮箱
	 * @param subject 邮件主题
	 * @param text    邮件内容
	 */
	ClientResponse sendEmail(String toEmail, String subject, String text);

}
