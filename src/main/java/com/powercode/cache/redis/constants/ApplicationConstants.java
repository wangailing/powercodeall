package com.powercode.cache.redis.constants;

/**
 * Redis的Key的前缀变量
 * 
 */
public final class ApplicationConstants {

	public static final String REDIS_PREFIX = "POWERCODE:";
	public static final String CHAR_CAPTCHA_PREFIX = "POWERCODE:CAPTCHA:USERNAME:";
	public static final String SMS_CAPTCHA_PREFIX = "POWERCODE:CAPTCHA:MOBILE:";
	public static final String URL_ROLECODE_PREFIX = "POWERCODE:URL:ROLECODE:";

	private ApplicationConstants() {

	}

}
