package com.powercode.chart.vo;

import java.io.Serializable;

import com.powercode.component.commons.vo.CommonVO;

/**
 * 图表信息的参数类
 * 
 */
public class ChartVO extends CommonVO implements Serializable {

	private static final long serialVersionUID = 7709247818109517872L;
	private String salesType;// 销售类别

	public String getSalesType() {
		return salesType;
	}

	public void setSalesType(String salesType) {
		this.salesType = salesType;
	}

}
