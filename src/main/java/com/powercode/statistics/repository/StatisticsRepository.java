package com.powercode.statistics.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.powercode.statistics.entity.Sms;

/**
 * 统计的数据持久接口层
 * 
 */
@Repository
public interface StatisticsRepository extends MongoRepository<Sms, String> {

}
