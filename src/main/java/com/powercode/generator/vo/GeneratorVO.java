package com.powercode.generator.vo;

import java.io.Serializable;

import com.powercode.component.commons.vo.CommonVO;

/**
 * 代码信息的参数类
 * 
 */
public class GeneratorVO extends CommonVO implements Serializable {

	private static final long serialVersionUID = 2994063747786522503L;
	private String entityName; // 实体名
	private String serviceName;// 服务名
	private String packageName;// 包名
	private String tableName;// 表名

	public String getEntityName() {
		return entityName;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

}
