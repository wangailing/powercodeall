package com.powercode;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ImportResource;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;

import com.bstek.ureport.console.UReportServlet;
import com.powercode.admin.mapper.SysRoleMapper;
import com.powercode.admin.mapper.SysUrlMapper;
import com.powercode.cache.redis.constants.ApplicationConstants;
import com.powercode.cache.redis.util.RedisUtils;
import com.powercode.gateway.filter.GatewayFilter;

/**
 * PowerCode的入口网关启动类
 *
 */

@SpringBootApplication(scanBasePackages = "com.powercode")
@EnableResourceServer
@EnableOAuth2Client
@EnableGlobalMethodSecurity(prePostEnabled = true)
@EnableDiscoveryClient
@EnableZuulProxy
@ImportResource("classpath:ureport-console-context.xml")
public class PowerCodeGatewayApplication implements CommandLineRunner {

	@Autowired
	private SysUrlMapper sysUrlMapper;
	@Autowired
	private SysRoleMapper sysRoleMapper;
	@Autowired
	private RedisUtils redisUtils;

	public static void main(String[] args) {
		SpringApplication.run(PowerCodeGatewayApplication.class);

	}

	@Bean
	public GatewayFilter gatewayFilter() {
		return new GatewayFilter();
	}

	@Bean
	public ServletRegistrationBean buildUReportServlet() {
		return new ServletRegistrationBean(new UReportServlet(), "/ureport/*");
	}

	/**
	 * 初始化角色编码对应的URL授权数据到Redis缓存，供网关验证权限
	 */
	@Override
	public void run(String... args) throws Exception {
		List<String> roleCodeList = sysRoleMapper.queryRoleCodeList();
		for (int i = 0; i < roleCodeList.size(); i++) {
			String roleCode = roleCodeList.get(i);
			List<String> url = sysUrlMapper.queryRoleUrl(roleCode);
			redisUtils.psetex(ApplicationConstants.URL_ROLECODE_PREFIX + roleCode, url == null ? Collections.emptyList().toString() : url.toString());
		}
	}

}
