package com.powercode.osce.entity;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;

import com.powercode.component.commons.entity.TimeEntity;
import com.powercode.component.commons.validator.UpdateValidator;
import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

/**
 * 实体类
 */
public class ExamPaperMain extends TimeEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	private long id;
	private String title;
	private String type;
	private String level;
	private Date generate_time;
	private int total_score;

	public void setId(long id) {
		this.id = id;
	}

	public long getId() {
		return id;
	}
	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitle() {
		return title;
	}
	public void setType(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}
	public void setLevel(String level) {
		this.level = level;
	}

	public String getLevel() {
		return level;
	}
	public void setGenerate_time(Date generate_time) {
		this.generate_time = generate_time;
	}

	public Date getGenerate_time() {
		return generate_time;
	}
	public void setTotal_score(int total_score) {
		this.total_score = total_score;
	}

	public int getTotal_score() {
		return total_score;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		ExamPaperMain item = (ExamPaperMain) o;
		return Objects.equal(id, item.id);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
			.add("id", id)
			.add("title", title)
			.add("type", type)
			.add("level", level)
			.add("generate_time", generate_time)
			.add("total_score", total_score)
			.toString();
	}

}
