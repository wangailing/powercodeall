package com.powercode.osce.entity;

import java.io.Serializable;
import java.util.Date;
import javax.validation.constraints.NotNull;

import com.powercode.component.commons.entity.TimeEntity;
import com.powercode.component.commons.validator.UpdateValidator;
import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

/**
 * 实体类
 */
public class WxUser extends TimeEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	private long id;
	private String wx_open_id;
	private String wx_skey;
	private Date wx_create_time;
	private Date wx_last_visit_time;
	private String wx_session_key;
	private String wx_city;
	private String wx_province;
	private String wx_country;
	private String wx_avatar_url;
	private int wx_gender;
	private String wx_nick_name;

	public void setId(long id) {
		this.id = id;
	}

	public long getId() {
		return id;
	}
	public void setWx_open_id(String wx_open_id) {
		this.wx_open_id = wx_open_id;
	}

	public String getWx_open_id() {
		return wx_open_id;
	}
	public void setWx_skey(String wx_skey) {
		this.wx_skey = wx_skey;
	}

	public String getWx_skey() {
		return wx_skey;
	}
	public void setWx_create_time(Date wx_create_time) {
		this.wx_create_time = wx_create_time;
	}

	public Date getWx_create_time() {
		return wx_create_time;
	}
	public void setWx_last_visit_time(Date wx_last_visit_time) {
		this.wx_last_visit_time = wx_last_visit_time;
	}

	public Date getWx_last_visit_time() {
		return wx_last_visit_time;
	}
	public void setWx_session_key(String wx_session_key) {
		this.wx_session_key = wx_session_key;
	}

	public String getWx_session_key() {
		return wx_session_key;
	}
	public void setWx_city(String wx_city) {
		this.wx_city = wx_city;
	}

	public String getWx_city() {
		return wx_city;
	}
	public void setWx_province(String wx_province) {
		this.wx_province = wx_province;
	}

	public String getWx_province() {
		return wx_province;
	}
	public void setWx_country(String wx_country) {
		this.wx_country = wx_country;
	}

	public String getWx_country() {
		return wx_country;
	}
	public void setWx_avatar_url(String wx_avatar_url) {
		this.wx_avatar_url = wx_avatar_url;
	}

	public String getWx_avatar_url() {
		return wx_avatar_url;
	}
	public void setWx_gender(int wx_gender) {
		this.wx_gender = wx_gender;
	}

	public int getWx_gender() {
		return wx_gender;
	}
	public void setWx_nick_name(String wx_nick_name) {
		this.wx_nick_name = wx_nick_name;
	}

	public String getWx_nick_name() {
		return wx_nick_name;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		WxUser item = (WxUser) o;
		return Objects.equal(wx_open_id, item.wx_open_id);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(wx_open_id);
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this).add("id", id)
			.add("wx_open_id", wx_open_id)
			.add("wx_skey", wx_skey)
			.add("wx_create_time", wx_create_time)
			.add("wx_last_visit_time", wx_last_visit_time)
			.add("wx_session_key", wx_session_key)
			.add("wx_city", wx_city)
			.add("wx_province", wx_province)
			.add("wx_country", wx_country)
			.add("wx_avatar_url", wx_avatar_url)
			.add("wx_gender", wx_gender)
			.add("wx_nick_name", wx_nick_name)
			.toString();
	}

}
