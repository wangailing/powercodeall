package com.powercode.osce.entity;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;

import com.powercode.component.commons.entity.TimeEntity;
import com.powercode.component.commons.validator.UpdateValidator;
import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

/**
 * 实体类
 */
public class ExamAudio extends TimeEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	private long id;
	private long user_id;
	private long station_id;
	private long task_id;
	private String url;
	private Date start_time;
	private Date end_time;

	public void setId(long id) {
		this.id = id;
	}

	public long getId() {
		return id;
	}
	public void setUser_id(long user_id) {
		this.user_id = user_id;
	}

	public long getUser_id() {
		return user_id;
	}
	public void setStation_id(long station_id) {
		this.station_id = station_id;
	}

	public long getStation_id() {
		return station_id;
	}
	public void setTask_id(long task_id) {
		this.task_id = task_id;
	}

	public long getTask_id() {
		return task_id;
	}
	public void setUrl(String url) {
		this.url = url;
	}

	public String getUrl() {
		return url;
	}
	public void setStart_time(Date start_time) {
		this.start_time = start_time;
	}

	public Date getStart_time() {
		return start_time;
	}
	public void setEnd_time(Date end_time) {
		this.end_time = end_time;
	}

	public Date getEnd_time() {
		return end_time;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		ExamAudio item = (ExamAudio) o;
		return Objects.equal(id, item.id);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
			.add("id", id)
			.add("user_id", user_id)
			.add("station_id", station_id)
			.add("task_id", task_id)
			.add("url", url)
			.add("start_time", start_time)
			.add("end_time", end_time)
			.toString();
	}

}
