package com.powercode.osce.entity;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import com.powercode.component.commons.entity.TimeEntity;
import com.powercode.component.commons.validator.UpdateValidator;
import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

/**
 * 实体类
 */
public class ExamProgress extends TimeEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	private long id;
	private long entity_id;
	private String entity_type;
	private long user_id;
	private int step_status;
	private int user_role;

	public long getEntity_id() {
		return entity_id;
	}

	public void setEntity_id(long entity_id) {
		this.entity_id = entity_id;
	}

	public String getEntity_type() {
		return entity_type;
	}

	public void setEntity_type(String entity_type) {
		this.entity_type = entity_type;
	}

	public int getStep_status() {
		return step_status;
	}

	public void setStep_status(int step_status) {
		this.step_status = step_status;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getId() {
		return id;
	}


	public void setUser_id(long user_id) {
		this.user_id = user_id;
	}

	public long getUser_id() {
		return user_id;
	}

	public void setUser_role(int user_role) {
		this.user_role = user_role;
	}

	public int getUser_role() {
		return user_role;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		ExamProgress item = (ExamProgress) o;
		return Objects.equal(id, item.id);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
			.add("id", id)
			.add("entity_id", entity_id)
			.add("entity_type", entity_type)
			.add("user_id", user_id)
			.add("step_status", step_status)
			.add("user_role", user_role)
			.toString();
	}

}
