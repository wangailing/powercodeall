package com.powercode.osce.entity;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.powercode.component.commons.entity.TimeEntity;
import com.powercode.component.commons.validator.UpdateValidator;
import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

/**
 * 实体类
 */

public class ExamTask extends TimeEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	private long id;
	private String subject;
	@JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
	private Date startTime;
	@JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
	private Date endTime;
	private long userId;
	private long station_num;

	public long getStation_num() {
		return station_num;
	}

	public void setStation_num(long station_num) {
		this.station_num = station_num;
	}

	private String descri;

	public void setId(long id) {
		this.id = id;
	}

	public long getId() {
		return id;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getSubject() {
		return subject;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getStartTime() {
		return startTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Date getEndTime() {
		return endTime;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}

	public long getUserId() {
		return userId;
	}
	public void setDescri(String descri) {
		this.descri = descri;
	}

	public String getDescri() {
		return descri;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		ExamTask item = (ExamTask) o;
		return Objects.equal(id, item.id);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
			.add("id", id)
			.add("subject", subject)
			.add("startTime", startTime)
			.add("endTime", endTime)
			.add("userId", userId)
			.add("descri", descri)
			.add("station_num", station_num)
			.toString();
	}

}
