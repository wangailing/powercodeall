package com.powercode.osce.entity;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import com.powercode.component.commons.entity.TimeEntity;
import com.powercode.component.commons.validator.UpdateValidator;
import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

/**
 * 实体类
 */
public class Evalrules extends TimeEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	private long id;
	private String role;
	private int type;
	private String interpretation;

	public void setId(long id) {
		this.id = id;
	}

	public long getId() {
		return id;
	}
	public void setRole(String role) {
		this.role = role;
	}

	public String getRole() {
		return role;
	}
	public void setType(int type) {
		this.type = type;
	}

	public int getType() {
		return type;
	}
	public void setInterpretation(String interpretation) {
		this.interpretation = interpretation;
	}

	public String getInterpretation() {
		return interpretation;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		Evalrules item = (Evalrules) o;
		return Objects.equal(id, item.id);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
			.add("id", id)
			.add("role", role)
			.add("type", type)
			.add("interpretation", interpretation)
			.toString();
	}

}
