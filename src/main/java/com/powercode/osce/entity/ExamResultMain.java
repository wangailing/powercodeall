package com.powercode.osce.entity;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;

import com.powercode.component.commons.entity.TimeEntity;
import com.powercode.component.commons.validator.UpdateValidator;
import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

/**
 * 实体类
 */
public class ExamResultMain extends TimeEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	private long id;
	private int exampaper_id;
	private int user_id;
	private int station_id;
	private Date start_time;
	private Date end_time;
	private int total_score;
	private String exam_audio;

	public void setId(long id) {
		this.id = id;
	}

	public long getId() {
		return id;
	}
	public void setExampaper_id(int exampaper_id) {
		this.exampaper_id = exampaper_id;
	}

	public int getExampaper_id() {
		return exampaper_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public int getUser_id() {
		return user_id;
	}
	public void setStation_id(int station_id) {
		this.station_id = station_id;
	}

	public int getStation_id() {
		return station_id;
	}
	public void setStart_time(Date start_time) {
		this.start_time = start_time;
	}

	public Date getStart_time() {
		return start_time;
	}
	public void setEnd_time(Date end_time) {
		this.end_time = end_time;
	}

	public Date getEnd_time() {
		return end_time;
	}
	public void setTotal_score(int total_score) {
		this.total_score = total_score;
	}

	public int getTotal_score() {
		return total_score;
	}
	public void setExam_audio(String exam_audio) {
		this.exam_audio = exam_audio;
	}

	public String getExam_audio() {
		return exam_audio;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		ExamResultMain item = (ExamResultMain) o;
		return Objects.equal(id, item.id);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
			.add("id", id)
			.add("exampaper_id", exampaper_id)
			.add("user_id", user_id)
			.add("station_id", station_id)
			.add("start_time", start_time)
			.add("end_time", end_time)
			.add("total_score", total_score)
			.add("exam_audio", exam_audio)
			.toString();
	}

}
