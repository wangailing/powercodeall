package com.powercode.osce.entity;

import java.io.Serializable;

import com.powercode.component.commons.entity.TimeEntity;
import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

/**
 * 实体类
 */
public class Cases extends TimeEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	private long id;
	private String osce_title;
	private String assistant_role;
	private String osce_object;
	private String scene;
	private String reference_answerr;
	private String alternative_answer;
	private int total_score;
	private int timelong;
	private String failure_reason;
	private String analysis;
	private String knowledge;
	private String skill;
	private int user_id;
	private int use_times;


	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public int getUse_times() {
		return use_times;
	}

	public void setUse_times(int use_times) {
		this.use_times = use_times;
	}
	public void setId(long id) {
		this.id = id;
	}

	public long getId() {
		return id;
	}
	public void setOsce_title(String osce_title) {
		this.osce_title = osce_title;
	}

	public String getOsce_title() {
		return osce_title;
	}
	public void setAssistant_role(String assistant_role) {
		this.assistant_role = assistant_role;
	}

	public String getAssistant_role() {
		return assistant_role;
	}
	public void setOsce_object(String osce_object) {
		this.osce_object = osce_object;
	}

	public String getOsce_object() {
		return osce_object;
	}
	public void setScene(String scene) {
		this.scene = scene;
	}

	public String getScene() {
		return scene;
	}
	public void setReference_answerr(String reference_answerr) {
		this.reference_answerr = reference_answerr;
	}

	public String getReference_answerr() {
		return reference_answerr;
	}
	public void setAlternative_answer(String alternative_answer) {
		this.alternative_answer = alternative_answer;
	}

	public String getAlternative_answer() {
		return alternative_answer;
	}
	public void setTotal_score(int total_score) {
		this.total_score = total_score;
	}

	public int getTotal_score() {
		return total_score;
	}
	public void setTimelong(int timelong) {
		this.timelong = timelong;
	}

	public int getTimelong() {
		return timelong;
	}
	public void setFailure_reason(String failure_reason) {
		this.failure_reason = failure_reason;
	}

	public String getFailure_reason() {
		return failure_reason;
	}
	public void setAnalysis(String analysis) {
		this.analysis = analysis;
	}

	public String getAnalysis() {
		return analysis;
	}
	public void setKnowledge(String knowledge) {
		this.knowledge = knowledge;
	}

	public String getKnowledge() {
		return knowledge;
	}
	public void setSkill(String skill) {
		this.skill = skill;
	}

	public String getSkill() {
		return skill;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		Cases item = (Cases) o;
		return Objects.equal(id, item.id);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
			.add("id", id)
			.add("osce_title", osce_title)
			.add("assistant_role", assistant_role)
			.add("osce_object", osce_object)
			.add("scene", scene)
			.add("reference_answerr", reference_answerr)
			.add("alternative_answer", alternative_answer)
			.add("total_score", total_score)
			.add("timelong", timelong)
			.add("failure_reason", failure_reason)
			.add("analysis", analysis)
			.add("knowledge", knowledge)
			.add("skill", skill)
			.add("examtask_id", user_id)
			.add("use_times", use_times)
			.toString();
	}

}
