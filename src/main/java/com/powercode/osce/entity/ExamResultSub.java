package com.powercode.osce.entity;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import com.powercode.component.commons.entity.TimeEntity;
import com.powercode.component.commons.validator.UpdateValidator;
import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

/**
 * 实体类
 */
public class ExamResultSub extends TimeEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	private long id;
	private int resultmain_id;
	private int papersub_id;
	private int score;
	private String evaluate;

	public void setId(long id) {
		this.id = id;
	}

	public long getId() {
		return id;
	}
	public void setResultmain_id(int resultmain_id) {
		this.resultmain_id = resultmain_id;
	}

	public int getResultmain_id() {
		return resultmain_id;
	}
	public void setPapersub_id(int papersub_id) {
		this.papersub_id = papersub_id;
	}

	public int getPapersub_id() {
		return papersub_id;
	}
	public void setScore(int score) {
		this.score = score;
	}

	public int getScore() {
		return score;
	}
	public void setEvaluate(String evaluate) {
		this.evaluate = evaluate;
	}

	public String getEvaluate() {
		return evaluate;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		ExamResultSub item = (ExamResultSub) o;
		return Objects.equal(id, item.id);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
			.add("id", id)
			.add("resultmain_id", resultmain_id)
			.add("papersub_id", papersub_id)
			.add("score", score)
			.add("evaluate", evaluate)
			.toString();
	}

}
