package com.powercode.osce.entity;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;

import com.powercode.component.commons.entity.TimeEntity;
import com.powercode.component.commons.validator.UpdateValidator;
import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

/**
 * 实体类
 */
public class ExamStation extends TimeEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	private long id;
	private String title;
	private Date plan_time;
	private String station_number;
	private long user_id;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getPlan_time() {
		return plan_time;
	}

	public void setPlan_time(Date plan_time) {
		this.plan_time = plan_time;
	}

	public String getStation_number() {
		return station_number;
	}

	public void setStation_number(String station_number) {
		this.station_number = station_number;
	}

	public long getUser_id() {
		return user_id;
	}

	public void setUser_id(long user_id) {
		this.user_id = user_id;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		ExamStation item = (ExamStation) o;
		return Objects.equal(id, item.id);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("id", id)
				.add("title", title)
				.add("plan_time", plan_time)
				.add("station_number", station_number)
				.add("user_id", user_id)
				.toString();
	}
}
