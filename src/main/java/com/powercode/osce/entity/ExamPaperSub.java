package com.powercode.osce.entity;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import com.powercode.component.commons.entity.TimeEntity;
import com.powercode.component.commons.validator.UpdateValidator;
import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

/**
 * 实体类
 */
public class ExamPaperSub extends TimeEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	private long id;
	private int papermain_id;
	private int title_id;
	private String osce_title;
	private int assistant_role;
	private int osce_object;
	private String scene;
	private String reference_answerr;
	private String alternative_answer;
	private int total_score;
	private int timelong;
	private String failure_reason;
	private String analysis;
	private String knowledge;
	private String skill;

	public void setId(long id) {
		this.id = id;
	}

	public long getId() {
		return id;
	}
	public void setPapermain_id(int papermain_id) {
		this.papermain_id = papermain_id;
	}

	public int getPapermain_id() {
		return papermain_id;
	}
	public void setTitle_id(int title_id) {
		this.title_id = title_id;
	}

	public int getTitle_id() {
		return title_id;
	}
	public void setOsce_title(String osce_title) {
		this.osce_title = osce_title;
	}

	public String getOsce_title() {
		return osce_title;
	}
	public void setAssistant_role(int assistant_role) {
		this.assistant_role = assistant_role;
	}

	public int getAssistant_role() {
		return assistant_role;
	}
	public void setOsce_object(int osce_object) {
		this.osce_object = osce_object;
	}

	public int getOsce_object() {
		return osce_object;
	}
	public void setScene(String scene) {
		this.scene = scene;
	}

	public String getScene() {
		return scene;
	}
	public void setReference_answerr(String reference_answerr) {
		this.reference_answerr = reference_answerr;
	}

	public String getReference_answerr() {
		return reference_answerr;
	}
	public void setAlternative_answer(String alternative_answer) {
		this.alternative_answer = alternative_answer;
	}

	public String getAlternative_answer() {
		return alternative_answer;
	}
	public void setTotal_score(int total_score) {
		this.total_score = total_score;
	}

	public int getTotal_score() {
		return total_score;
	}
	public void setTimelong(int timelong) {
		this.timelong = timelong;
	}

	public int getTimelong() {
		return timelong;
	}
	public void setFailure_reason(String failure_reason) {
		this.failure_reason = failure_reason;
	}

	public String getFailure_reason() {
		return failure_reason;
	}
	public void setAnalysis(String analysis) {
		this.analysis = analysis;
	}

	public String getAnalysis() {
		return analysis;
	}
	public void setKnowledge(String knowledge) {
		this.knowledge = knowledge;
	}

	public String getKnowledge() {
		return knowledge;
	}
	public void setSkill(String skill) {
		this.skill = skill;
	}

	public String getSkill() {
		return skill;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		ExamPaperSub item = (ExamPaperSub) o;
		return Objects.equal(id, item.id);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
			.add("id", id)
			.add("papermain_id", papermain_id)
			.add("title_id", title_id)
			.add("osce_title", osce_title)
			.add("assistant_role", assistant_role)
			.add("osce_object", osce_object)
			.add("scene", scene)
			.add("reference_answerr", reference_answerr)
			.add("alternative_answer", alternative_answer)
			.add("total_score", total_score)
			.add("timelong", timelong)
			.add("failure_reason", failure_reason)
			.add("analysis", analysis)
			.add("knowledge", knowledge)
			.add("skill", skill)
			.toString();
	}

}
