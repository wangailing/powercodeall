package com.powercode.osce.vo;

import java.io.Serializable;
import java.util.Date;

import com.powercode.component.commons.vo.CommonVO;

/**
 * 参数类
 */
public class ExamStationVO extends CommonVO implements Serializable {

	private static final long serialVersionUID = 1L;
	private long id;
	private String title;
	private Date plan_time;
	private String station_number;
	private long user_id;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getPlan_time() {
		return plan_time;
	}

	public void setPlan_time(Date plan_time) {
		this.plan_time = plan_time;
	}

	public String getStation_number() {
		return station_number;
	}

	public void setStation_number(String station_number) {
		this.station_number = station_number;
	}

	public long getUser_id() {
		return user_id;
	}

	public void setUser_id(long user_id) {
		this.user_id = user_id;
	}


}
