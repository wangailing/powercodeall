package com.powercode.osce.vo;

import java.io.Serializable;
import java.util.Date;

import com.powercode.component.commons.vo.CommonVO;


/**
 * 参数类
 */
public class ExamTaskVO extends CommonVO implements Serializable {


    private static final long serialVersionUID = 1L;
    private long id;
    private String subject;
    private Date startTime;
    private Date endTime;
    private long userId;
    private String descri;
    private long station_num;

    public long getStation_num() {
        return station_num;
    }

    public void setStation_num(long station_num) {
        this.station_num = station_num;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getSubject() {
        return subject;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getUserId() {
        return userId;
    }

    public void setDescri(String descri) {
        this.descri = descri;
    }

    public String getDescri() {
        return descri;
    }

}
