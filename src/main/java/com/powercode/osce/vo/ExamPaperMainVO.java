package com.powercode.osce.vo;

import java.io.Serializable;
import java.util.Date;

import com.powercode.component.commons.vo.CommonVO;

/**
 * 参数类
 */
public class ExamPaperMainVO extends CommonVO implements Serializable {

	private static final long serialVersionUID = 1L;
	private long id;
	private String title;
	private String type;
	private String level;
	private Date generate_time;
	private int total_score;

	public void setId(long id) {
		this.id = id;
	}

	public long getId() {
		return id;
	}
	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitle() {
		return title;
	}
	public void setType(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}
	public void setLevel(String level) {
		this.level = level;
	}

	public String getLevel() {
		return level;
	}
	public void setGenerate_time(Date generate_time) {
		this.generate_time = generate_time;
	}

	public Date getGenerate_time() {
		return generate_time;
	}
	public void setTotal_score(int total_score) {
		this.total_score = total_score;
	}

	public int getTotal_score() {
		return total_score;
	}

}
