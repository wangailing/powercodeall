package com.powercode.osce.vo;

import java.io.Serializable;

import com.powercode.component.commons.vo.CommonVO;

/**
 * 参数类
 */
public class ExamPaperSubVO extends CommonVO implements Serializable {

	private static final long serialVersionUID = 1L;
	private long id;
	private int papermain_id;
	private int title_id;
	private String osce_title;
	private int assistant_role;
	private int osce_object;
	private String scene;
	private String reference_answerr;
	private String alternative_answer;
	private int total_score;
	private int timelong;
	private String failure_reason;
	private String analysis;
	private String knowledge;
	private String skill;

	public void setId(long id) {
		this.id = id;
	}

	public long getId() {
		return id;
	}
	public void setPapermain_id(int papermain_id) {
		this.papermain_id = papermain_id;
	}

	public int getPapermain_id() {
		return papermain_id;
	}
	public void setTitle_id(int title_id) {
		this.title_id = title_id;
	}

	public int getTitle_id() {
		return title_id;
	}
	public void setOsce_title(String osce_title) {
		this.osce_title = osce_title;
	}

	public String getOsce_title() {
		return osce_title;
	}
	public void setAssistant_role(int assistant_role) {
		this.assistant_role = assistant_role;
	}

	public int getAssistant_role() {
		return assistant_role;
	}
	public void setOsce_object(int osce_object) {
		this.osce_object = osce_object;
	}

	public int getOsce_object() {
		return osce_object;
	}
	public void setScene(String scene) {
		this.scene = scene;
	}

	public String getScene() {
		return scene;
	}
	public void setReference_answerr(String reference_answerr) {
		this.reference_answerr = reference_answerr;
	}

	public String getReference_answerr() {
		return reference_answerr;
	}
	public void setAlternative_answer(String alternative_answer) {
		this.alternative_answer = alternative_answer;
	}

	public String getAlternative_answer() {
		return alternative_answer;
	}
	public void setTotal_score(int total_score) {
		this.total_score = total_score;
	}

	public int getTotal_score() {
		return total_score;
	}
	public void setTimelong(int timelong) {
		this.timelong = timelong;
	}

	public int getTimelong() {
		return timelong;
	}
	public void setFailure_reason(String failure_reason) {
		this.failure_reason = failure_reason;
	}

	public String getFailure_reason() {
		return failure_reason;
	}
	public void setAnalysis(String analysis) {
		this.analysis = analysis;
	}

	public String getAnalysis() {
		return analysis;
	}
	public void setKnowledge(String knowledge) {
		this.knowledge = knowledge;
	}

	public String getKnowledge() {
		return knowledge;
	}
	public void setSkill(String skill) {
		this.skill = skill;
	}

	public String getSkill() {
		return skill;
	}

}
