package com.powercode.osce.vo;

import java.io.Serializable;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import com.powercode.component.commons.vo.CommonVO;
import com.powercode.osce.entity.ExamProgress;

/**
 * 参数类
 */
public class ExamProgressVO extends CommonVO implements Serializable {

	private static final long serialVersionUID = 1L;
	private long id;
	private long entity_id;
	private long entity_type;
	private long user_id;
	private int step_status;
	private int user_role;

	public long getEntity_id() {
		return entity_id;
	}

	public void setEntity_id(long entity_id) {
		this.entity_id = entity_id;
	}

	public long getEntity_type() {
		return entity_type;
	}

	public void setEntity_type(long entity_type) {
		this.entity_type = entity_type;
	}

	public int getStep_status() {
		return step_status;
	}

	public void setStep_status(int step_status) {
		this.step_status = step_status;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getId() {
		return id;
	}


	public void setUser_id(long user_id) {
		this.user_id = user_id;
	}

	public long getUser_id() {
		return user_id;
	}

	public void setUser_role(int user_role) {
		this.user_role = user_role;
	}

	public int getUser_role() {
		return user_role;
	}

}
