package com.powercode.osce.vo;

import java.io.Serializable;
import java.util.Date;

import com.powercode.component.commons.vo.CommonVO;

/**
 * 参数类
 */
public class PointsVO extends CommonVO implements Serializable {

	private static final long serialVersionUID = 1L;
	private long id;
	private int score;
	private int hospital_id;
	private int department_id;
	private int score_source;

	private long user_id;

	public long getUser_id() {
		return user_id;
	}

	public void setUser_id(long user_id) {
		this.user_id = user_id;
	}

	public int getScore_source() {
		return score_source;
	}

	public void setScore_source(int score_source) {
		this.score_source = score_source;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getId() {
		return id;
	}
	public void setScore(int score) {
		this.score = score;
	}

	public int getScore() {
		return score;
	}
	public void setHospital_id(int hospital_id) {
		this.hospital_id = hospital_id;
	}

	public int getHospital_id() {
		return hospital_id;
	}
	public void setDepartment_id(int department_id) {
		this.department_id = department_id;
	}

	public int getDepartment_id() {
		return department_id;
	}

}
