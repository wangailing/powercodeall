package com.powercode.osce.vo;

import java.io.Serializable;

import com.google.common.base.Objects;
import com.powercode.component.commons.vo.CommonVO;
import com.powercode.osce.entity.Evalrules;

/**
 * 参数类
 */
public class EvalrulesVO extends CommonVO implements Serializable {

	private static final long serialVersionUID = 1L;
	private long id;
	private String role;
	private int type;
	private String interpretation;

	public void setId(long id) {
		this.id = id;
	}

	public long getId() {
		return id;
	}
	public void setRole(String role) {
		this.role = role;
	}

	public String getRole() {
		return role;
	}
	public void setType(int type) {
		this.type = type;
	}

	public int getType() {
		return type;
	}
	public void setInterpretation(String interpretation) {
		this.interpretation = interpretation;
	}

	public String getInterpretation() {
		return interpretation;
	}



}
