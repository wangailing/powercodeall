package com.powercode.osce.vo;

import java.io.Serializable;
import java.security.Timestamp;
import java.util.Date;

import com.powercode.component.commons.vo.CommonVO;

/**
 * 参数类
 */
public class WxUserVO extends CommonVO implements Serializable {

	private static final long serialVersionUID = 1L;
	private long id;
	private String wx_open_id;
	private String wx_skey;
	private Date wx_create_time;
	private Date wx_last_visit_time;
	private String wx_session_key;
	private String wx_city;
	private String wx_province;
	private String wx_country;
	private String wx_avatar_url;
	private int wx_gender;
	private String wx_nick_name;

	public void setId(long id) {
		this.id = id;
	}

	public long getId() {
		return id;
	}
	public void setWx_open_id(String wx_open_id) {
		this.wx_open_id = wx_open_id;
	}

	public String getWx_open_id() {
		return wx_open_id;
	}
	public void setWx_skey(String wx_skey) {
		this.wx_skey = wx_skey;
	}

	public String getWx_skey() {
		return wx_skey;
	}
	public void setWx_create_time(Date wx_create_time) {
		this.wx_create_time = wx_create_time;
	}

	public Date getWx_create_time() {
		return wx_create_time;
	}
	public void setWx_last_visit_time(Date wx_last_visit_time) {
		this.wx_last_visit_time = wx_last_visit_time;
	}

	public Date getWx_last_visit_time() {
		return wx_last_visit_time;
	}
	public void setWx_session_key(String wx_session_key) {
		this.wx_session_key = wx_session_key;
	}

	public String getWx_session_key() {
		return wx_session_key;
	}
	public void setWx_city(String wx_city) {
		this.wx_city = wx_city;
	}

	public String getWx_city() {
		return wx_city;
	}
	public void setWx_province(String wx_province) {
		this.wx_province = wx_province;
	}

	public String getWx_province() {
		return wx_province;
	}
	public void setWx_country(String wx_country) {
		this.wx_country = wx_country;
	}

	public String getWx_country() {
		return wx_country;
	}
	public void setWx_avatar_url(String wx_avatar_url) {
		this.wx_avatar_url = wx_avatar_url;
	}

	public String getWx_avatar_url() {
		return wx_avatar_url;
	}
	public void setWx_gender(int wx_gender) {
		this.wx_gender = wx_gender;
	}

	public int getWx_gender() {
		return wx_gender;
	}
	public void setWx_nick_name(String wx_nick_name) {
		this.wx_nick_name = wx_nick_name;
	}

	public String getWx_nick_name() {
		return wx_nick_name;
	}

}
