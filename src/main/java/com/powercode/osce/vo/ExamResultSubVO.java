package com.powercode.osce.vo;

import java.io.Serializable;

import com.powercode.component.commons.vo.CommonVO;

/**
 * 参数类
 */
public class ExamResultSubVO extends CommonVO implements Serializable {

	private static final long serialVersionUID = 1L;
	private long id;
	private int resultmain_id;
	private int papersub_id;
	private int score;
	private String evaluate;

	public void setId(long id) {
		this.id = id;
	}

	public long getId() {
		return id;
	}
	public void setResultmain_id(int resultmain_id) {
		this.resultmain_id = resultmain_id;
	}

	public int getResultmain_id() {
		return resultmain_id;
	}
	public void setPapersub_id(int papersub_id) {
		this.papersub_id = papersub_id;
	}

	public int getPapersub_id() {
		return papersub_id;
	}
	public void setScore(int score) {
		this.score = score;
	}

	public int getScore() {
		return score;
	}
	public void setEvaluate(String evaluate) {
		this.evaluate = evaluate;
	}

	public String getEvaluate() {
		return evaluate;
	}

}
