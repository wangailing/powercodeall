package com.powercode.osce.service;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.powercode.osce.entity.ExamResultSub;

/**
 * 业务逻辑接口层
 */
public interface ExamResultSubService {

	/**
	 * 查询分页
	 * 
	 * @param currentPage   当前页数
	 * @param pageSize      每页记录数
	 * @param sorter        排序
	 * @return
	 */
	Map<String, Object> queryExamResultSub(Integer currentPage, Integer pageSize, String sorter);

	/**
	 * 查询导出数据列表
	 * 
	 * @param paramMap 参数Map
	 * @return
	 */
	List<LinkedHashMap<String, Object>> queryExamResultSubForExcel(Map<String, Object> paramMap);

	/**
	 * 新增
	 * 
	 * @param examResultSub 对象
	 */
	void insertExamResultSub(ExamResultSub examResultSub);

	/**
	 * 编辑
	 * 
	 * @param examResultSub 对象
	 */
	void updateExamResultSub(ExamResultSub examResultSub);

	/**
	 * 删除
	 * 
	 * @param id ID
	 */
	void deleteExamResultSub(Long[] id);

}
