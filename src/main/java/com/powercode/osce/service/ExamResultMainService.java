package com.powercode.osce.service;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.powercode.osce.entity.ExamResultMain;

/**
 * 业务逻辑接口层
 */
public interface ExamResultMainService {

	/**
	 * 查询分页
	 * 
	 * @param currentPage   当前页数
	 * @param pageSize      每页记录数
	 * @param sorter        排序
	 * @return
	 */
	Map<String, Object> queryExamResultMain(Integer currentPage, Integer pageSize, String sorter);

	/**
	 * 查询导出数据列表
	 * 
	 * @param paramMap 参数Map
	 * @return
	 */
	List<LinkedHashMap<String, Object>> queryExamResultMainForExcel(Map<String, Object> paramMap);

	/**
	 * 新增
	 * 
	 * @param examResultMain 对象
	 */
	void insertExamResultMain(ExamResultMain examResultMain);

	/**
	 * 编辑
	 * 
	 * @param examResultMain 对象
	 */
	void updateExamResultMain(ExamResultMain examResultMain);

	/**
	 * 删除
	 * 
	 * @param id ID
	 */
	void deleteExamResultMain(Long[] id);

}
