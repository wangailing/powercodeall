package com.powercode.osce.service;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.powercode.component.commons.wxcommons.Auth;
import com.powercode.component.commons.wxcommons.Result;
import com.powercode.osce.entity.WxUser;

/**
 * 业务逻辑接口层
 */
public interface WxUserService {

	/**
	 * 查询分页
	 * 
	 * @param currentPage   当前页数
	 * @param pageSize      每页记录数
	 * @param sorter        排序
	 * @return
	 */
	Map<String, Object> queryWxUser(Integer currentPage, Integer pageSize, String sorter);

	/**
	 * 查询导出数据列表
	 * 
	 * @param paramMap 参数Map
	 * @return
	 */
	List<LinkedHashMap<String, Object>> queryWxUserForExcel(Map<String, Object> paramMap);

	/**
	 * 新增
	 * 
	 * @param wxUser 对象
	 */
	void insertWxUser(WxUser wxUser);

	/**
	 * 编辑
	 * 
	 * @param wxUser 对象
	 */
	void updateWxUser(WxUser wxUser);

	/**
	 * 删除
	 * 
	 * @param id ID
	 */
	void deleteWxUser(Long[] id);

	/**
	 * sessionId
	 *
	 * @param code CODE
	 */
    String getSessionId(String code);

	/**
	 * auth
	 *
	 * @param auth AUTH
	 */
	Result wxUserLogin(Auth auth);
}
