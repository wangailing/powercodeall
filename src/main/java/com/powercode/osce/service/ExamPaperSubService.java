package com.powercode.osce.service;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.powercode.osce.entity.ExamPaperSub;

/**
 * 业务逻辑接口层
 */
public interface ExamPaperSubService {

	/**
	 * 查询分页
	 * 
	 * @param currentPage   当前页数
	 * @param pageSize      每页记录数
	 * @param sorter        排序
	 * @return
	 */
	Map<String, Object> queryExamPaperSub(Integer currentPage, Integer pageSize, String sorter);

	/**
	 * 查询导出数据列表
	 * 
	 * @param paramMap 参数Map
	 * @return
	 */
	List<LinkedHashMap<String, Object>> queryExamPaperSubForExcel(Map<String, Object> paramMap);

	/**
	 * 新增
	 * 
	 * @param examPaperSub 对象
	 */
	void insertExamPaperSub(ExamPaperSub examPaperSub);

	/**
	 * 编辑
	 * 
	 * @param examPaperSub 对象
	 */
	void updateExamPaperSub(ExamPaperSub examPaperSub);

	/**
	 * 删除
	 * 
	 * @param id ID
	 */
	void deleteExamPaperSub(Long[] id);

}
