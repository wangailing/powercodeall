package com.powercode.osce.service;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.powercode.osce.entity.ExamPaperMain;

/**
 * 业务逻辑接口层
 */
public interface ExamPaperMainService {

	/**
	 * 查询分页
	 * 
	 * @param currentPage   当前页数
	 * @param pageSize      每页记录数
	 * @param sorter        排序
	 * @return
	 */
	Map<String, Object> queryExamPaperMain(Integer currentPage, Integer pageSize, String sorter);

	/**
	 * 查询导出数据列表
	 * 
	 * @param paramMap 参数Map
	 * @return
	 */
	List<LinkedHashMap<String, Object>> queryExamPaperMainForExcel(Map<String, Object> paramMap);

	/**
	 * 新增
	 * 
	 * @param examPaperMain 对象
	 */
	void insertExamPaperMain(ExamPaperMain examPaperMain);

	/**
	 * 编辑
	 * 
	 * @param examPaperMain 对象
	 */
	void updateExamPaperMain(ExamPaperMain examPaperMain);

	/**
	 * 删除
	 * 
	 * @param id ID
	 */
	void deleteExamPaperMain(Long[] id);

}
