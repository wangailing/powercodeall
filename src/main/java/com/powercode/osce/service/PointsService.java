package com.powercode.osce.service;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.powercode.osce.entity.Points;

/**
 * 业务逻辑接口层
 */
public interface PointsService {

	/**
	 * 查询分页
	 * 
	 * @param currentPage   当前页数
	 * @param pageSize      每页记录数
	 * @param sorter        排序
	 * @return
	 */
	Map<String, Object> queryPoints(Integer currentPage, Integer pageSize, String sorter);

	/**
	 * 查询导出数据列表
	 * 
	 * @param paramMap 参数Map
	 * @return
	 */
	List<LinkedHashMap<String, Object>> queryPointsForExcel(Map<String, Object> paramMap);

	/**
	 * 新增
	 * 
	 * @param points 对象
	 */
	void insertPoints(Points points);

	/**
	 * 编辑
	 * 
	 * @param points 对象
	 */
	void updatePoints(Points points);

	/**
	 * 删除
	 * 
	 * @param id ID
	 */
	void deletePoints(Long[] id);

}
