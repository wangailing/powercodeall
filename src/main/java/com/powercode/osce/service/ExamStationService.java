package com.powercode.osce.service;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.powercode.osce.entity.ExamStation;

/**
 * 业务逻辑接口层
 */
public interface ExamStationService {

	/**
	 * 查询分页
	 * 
	 * @param currentPage   当前页数
	 * @param pageSize      每页记录数
	 * @param sorter        排序
	 * @return
	 */
	Map<String, Object> queryExamStation(Integer currentPage, Integer pageSize, String sorter);

	/**
	 * 查询导出数据列表
	 * 
	 * @param paramMap 参数Map
	 * @return
	 */
	List<LinkedHashMap<String, Object>> queryExamStationForExcel(Map<String, Object> paramMap);

	/**
	 * 新增
	 * 
	 * @param examStation 对象
	 */
	void insertExamStation(ExamStation examStation);

	/**
	 * 编辑
	 * 
	 * @param examStation 对象
	 */
	void updateExamStation(ExamStation examStation);

	/**
	 * 删除
	 * 
	 * @param id ID
	 */
	void deleteExamStation(Long[] id);

}
