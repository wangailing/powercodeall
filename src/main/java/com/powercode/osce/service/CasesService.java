package com.powercode.osce.service;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.powercode.osce.entity.Cases;

/**
 * 业务逻辑接口层
 */
public interface CasesService {

	/**
	 * 查询分页
	 * 
	 * @param currentPage   当前页数
	 * @param pageSize      每页记录数
	 * @param sorter        排序
	 * @return
	 */
	Map<String, Object> queryCases(Integer currentPage, Integer pageSize, String sorter);

	/**
	 * 查询导出数据列表
	 * 
	 * @param paramMap 参数Map
	 * @return
	 */
	List<LinkedHashMap<String, Object>> queryCasesForExcel(Map<String, Object> paramMap);

	/**
	 * 新增
	 * 
	 * @param cases 对象
	 */
	void insertCases(Cases cases);

	/**
	 * 编辑
	 * 
	 * @param cases 对象
	 */
	void updateCases(Cases cases);

	/**
	 * 删除
	 * 
	 * @param id ID
	 */
	void deleteCases(Long[] id);



	/**
	 * 查询机构类型的下拉框数据
	 *
	 * @return
	 */
	LinkedHashMap<String, Object> queryExamType();

}
