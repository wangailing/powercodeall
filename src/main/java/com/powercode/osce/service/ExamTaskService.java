package com.powercode.osce.service;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.powercode.osce.entity.ExamTask;

/**
 * 业务逻辑接口层
 */
public interface ExamTaskService {

	/**
	 * 查询分页
	 * 
	 * @param currentPage   当前页数
	 * @param pageSize      每页记录数
	 * @param sorter        排序
	 * @return
	 */
	Map<String, Object> queryExamTask(Integer currentPage, Integer pageSize, String sorter);

	/**
	 * 查询导出数据列表
	 * 
	 * @param paramMap 参数Map
	 * @return
	 */
	List<LinkedHashMap<String, Object>> queryExamTaskForExcel(Map<String, Object> paramMap);

	/**
	 * 新增
	 * 
	 * @param examTask 对象
	 */
	long insertExamTask(ExamTask examTask);

	/**
	 * 编辑
	 * 
	 * @param examTask 对象
	 */
	void updateExamTask(ExamTask examTask);

	/**
	 * 删除
	 * 
	 * @param id ID
	 */
	void deleteExamTask(Long[] id);

}
