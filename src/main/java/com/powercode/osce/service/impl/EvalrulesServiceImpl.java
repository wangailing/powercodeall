package com.powercode.osce.service.impl;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.powercode.osce.entity.Evalrules;
import com.powercode.admin.mapper.SysRoleMapper;
import com.powercode.osce.mapper.EvalrulesMapper;
import com.powercode.osce.service.EvalrulesService;
import com.powercode.component.commons.result.PaginationBuilder;
import com.powercode.component.commons.utils.CollectionUtils;
import com.powercode.component.commons.utils.SequenceGenerator;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

/**
 * 业务逻辑实现层
 */
@Service
@Transactional
public class EvalrulesServiceImpl implements EvalrulesService {

	private final Logger logger = LoggerFactory.getLogger(getClass());
	private static SequenceGenerator sequenceGenerator = new SequenceGenerator();

	@Autowired
	private EvalrulesMapper evalrulesMapper;
	@Autowired
	private SysRoleMapper sysRoleMapper;

	/**
	 * 查询分页
	 */
	@Override
	public Map<String, Object> queryEvalrules(Integer currentPage, Integer pageSize, String sorter) {
		Map<String, Object> paramMap = new HashMap<>();
		if (StringUtils.isNotBlank(sorter)) {
			String sort = sorter.substring(0, sorter.lastIndexOf('_'));
			String sequence = "ascend".equals(sorter.substring(sorter.lastIndexOf('_') + 1)) ? "ASC" : "DESC";
			paramMap.put("sort", sort);
			paramMap.put("sequence", sequence);
		} else {
			paramMap.put("sort", "createTime");
			paramMap.put("sequence", "DESC");
		}
		Page<Object> page = PageHelper.startPage(currentPage, pageSize);
		List<LinkedHashMap<String, Object>> resultList = evalrulesMapper.queryEvalrules(paramMap);

		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String roleData = sysRoleMapper.queryRoleData("evalrules", authentication.getName());
		String[] roleDataArray = roleData == null ? null : roleData.split(",");
		if (roleDataArray != null && roleDataArray.length > 0) {// 处理数据权限
			return PaginationBuilder.buildResult(CollectionUtils.convertFilterList(resultList, roleDataArray), page.getTotal(), currentPage, pageSize);
		} else {
			return PaginationBuilder.buildResult(resultList, page.getTotal(), currentPage, pageSize);
		}
	}

	/**
	 * 查询导出数据列表
	 */
	@Override
	public List<LinkedHashMap<String, Object>> queryEvalrulesForExcel(Map<String, Object> paramMap) {
		return evalrulesMapper.queryEvalrules(paramMap);
	}

	/**
	 * 新增
	 */
	@Override
	public void insertEvalrules(Evalrules evalrules) {
		evalrules.setId(sequenceGenerator.nextId());
		evalrulesMapper.insertEvalrules(evalrules);
		logger.info("已新增： {}", evalrules.getId());
	}

	/**
	 * 编辑
	 */
	@Override
	public void updateEvalrules(Evalrules evalrules) {
		evalrulesMapper.updateEvalrules(evalrules);
		logger.info("已编辑： {}", evalrules.getId());
	}

	/**
	 * 删除
	 */
	@Override
	public void deleteEvalrules(Long[] id) {
		evalrulesMapper.deleteEvalrules(id);
	}

}
