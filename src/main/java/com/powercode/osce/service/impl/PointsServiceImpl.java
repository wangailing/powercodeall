package com.powercode.osce.service.impl;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.powercode.osce.entity.Points;
import com.powercode.admin.mapper.SysRoleMapper;
import com.powercode.osce.mapper.PointsMapper;
import com.powercode.osce.service.PointsService;
import com.powercode.component.commons.result.PaginationBuilder;
import com.powercode.component.commons.utils.CollectionUtils;
import com.powercode.component.commons.utils.SequenceGenerator;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

/**
 * 业务逻辑实现层
 */
@Service
@Transactional
public class PointsServiceImpl implements PointsService {

	private final Logger logger = LoggerFactory.getLogger(getClass());
	private static SequenceGenerator sequenceGenerator = new SequenceGenerator();

	@Autowired
	private PointsMapper pointsMapper;
	@Autowired
	private SysRoleMapper sysRoleMapper;

	/**
	 * 查询分页
	 */
	@Override
	public Map<String, Object> queryPoints(Integer currentPage, Integer pageSize, String sorter) {
		Map<String, Object> paramMap = new HashMap<>();
		if (StringUtils.isNotBlank(sorter)) {
			String sort = sorter.substring(0, sorter.lastIndexOf('_'));
			String sequence = "ascend".equals(sorter.substring(sorter.lastIndexOf('_') + 1)) ? "ASC" : "DESC";
			paramMap.put("sort", sort);
			paramMap.put("sequence", sequence);
		} else {
			paramMap.put("sort", "createTime");
			paramMap.put("sequence", "DESC");
		}
		Page<Object> page = PageHelper.startPage(currentPage, pageSize);
		List<LinkedHashMap<String, Object>> resultList = pointsMapper.queryPoints(paramMap);

		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String roleData = sysRoleMapper.queryRoleData("points", authentication.getName());
		String[] roleDataArray = roleData == null ? null : roleData.split(",");
		if (roleDataArray != null && roleDataArray.length > 0) {// 处理数据权限
			return PaginationBuilder.buildResult(CollectionUtils.convertFilterList(resultList, roleDataArray), page.getTotal(), currentPage, pageSize);
		} else {
			return PaginationBuilder.buildResult(resultList, page.getTotal(), currentPage, pageSize);
		}
	}

	/**
	 * 查询导出数据列表
	 */
	@Override
	public List<LinkedHashMap<String, Object>> queryPointsForExcel(Map<String, Object> paramMap) {
		return pointsMapper.queryPoints(paramMap);
	}

	/**
	 * 新增
	 */
	@Override
	public void insertPoints(Points points) {
		points.setId(sequenceGenerator.nextId());
		pointsMapper.insertPoints(points);
		logger.info("已新增： {}", points.getId());
	}

	/**
	 * 编辑
	 */
	@Override
	public void updatePoints(Points points) {
		pointsMapper.updatePoints(points);
		logger.info("已编辑： {}", points.getId());
	}

	/**
	 * 删除
	 */
	@Override
	public void deletePoints(Long[] id) {
		pointsMapper.deletePoints(id);
	}

}
