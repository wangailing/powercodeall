package com.powercode.osce.service.impl;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.powercode.osce.entity.ExamPaperSub;
import com.powercode.admin.mapper.SysRoleMapper;
import com.powercode.osce.mapper.ExamPaperSubMapper;
import com.powercode.osce.service.ExamPaperSubService;
import com.powercode.component.commons.result.PaginationBuilder;
import com.powercode.component.commons.utils.CollectionUtils;
import com.powercode.component.commons.utils.SequenceGenerator;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

/**
 * 业务逻辑实现层
 */
@Service
@Transactional
public class ExamPaperSubServiceImpl implements ExamPaperSubService {

	private final Logger logger = LoggerFactory.getLogger(getClass());
	private static SequenceGenerator sequenceGenerator = new SequenceGenerator();

	@Autowired
	private ExamPaperSubMapper examPaperSubMapper;
	@Autowired
	private SysRoleMapper sysRoleMapper;

	/**
	 * 查询分页
	 */
	@Override
	public Map<String, Object> queryExamPaperSub(Integer currentPage, Integer pageSize, String sorter) {
		Map<String, Object> paramMap = new HashMap<>();
		if (StringUtils.isNotBlank(sorter)) {
			String sort = sorter.substring(0, sorter.lastIndexOf('_'));
			String sequence = "ascend".equals(sorter.substring(sorter.lastIndexOf('_') + 1)) ? "ASC" : "DESC";
			paramMap.put("sort", sort);
			paramMap.put("sequence", sequence);
		} else {
			paramMap.put("sort", "createTime");
			paramMap.put("sequence", "DESC");
		}
		Page<Object> page = PageHelper.startPage(currentPage, pageSize);
		List<LinkedHashMap<String, Object>> resultList = examPaperSubMapper.queryExamPaperSub(paramMap);

		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String roleData = sysRoleMapper.queryRoleData("exampapersub", authentication.getName());
		String[] roleDataArray = roleData == null ? null : roleData.split(",");
		if (roleDataArray != null && roleDataArray.length > 0) {// 处理数据权限
			return PaginationBuilder.buildResult(CollectionUtils.convertFilterList(resultList, roleDataArray), page.getTotal(), currentPage, pageSize);
		} else {
			return PaginationBuilder.buildResult(resultList, page.getTotal(), currentPage, pageSize);
		}
	}

	/**
	 * 查询导出数据列表
	 */
	@Override
	public List<LinkedHashMap<String, Object>> queryExamPaperSubForExcel(Map<String, Object> paramMap) {
		return examPaperSubMapper.queryExamPaperSub(paramMap);
	}

	/**
	 * 新增
	 */
	@Override
	public void insertExamPaperSub(ExamPaperSub examPaperSub) {
		examPaperSub.setId(sequenceGenerator.nextId());
		examPaperSubMapper.insertExamPaperSub(examPaperSub);
		logger.info("已新增： {}", examPaperSub.getId());
	}

	/**
	 * 编辑
	 */
	@Override
	public void updateExamPaperSub(ExamPaperSub examPaperSub) {
		examPaperSubMapper.updateExamPaperSub(examPaperSub);
		logger.info("已编辑： {}", examPaperSub.getId());
	}

	/**
	 * 删除
	 */
	@Override
	public void deleteExamPaperSub(Long[] id) {
		examPaperSubMapper.deleteExamPaperSub(id);
	}

}
