package com.powercode.osce.service.impl;
import com.alibaba.fastjson.JSON;
import java.util.*;
import cn.hutool.core.lang.UUID;
import cn.hutool.http.HttpUtil;
import com.powercode.component.commons.wxcommons.Auth;
import com.powercode.component.commons.wxcommons.Result;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.slf4j.LoggerFactory;
import com.powercode.osce.entity.WxUser;
import com.powercode.admin.mapper.SysRoleMapper;
import com.powercode.osce.mapper.WxUserMapper;
import com.powercode.osce.service.WxUserService;
import com.powercode.component.commons.result.PaginationBuilder;
import com.powercode.component.commons.utils.CollectionUtils;
import com.powercode.component.commons.utils.SequenceGenerator;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

/**
 * 业务逻辑实现层
 */
@Service
@Transactional
public class WxUserServiceImpl implements WxUserService {

	private final Logger logger = LoggerFactory.getLogger(getClass());
	private static SequenceGenerator sequenceGenerator = new SequenceGenerator();

	@Value("${weixin.appid}")
	private String appid;
	
	@Value("${weixin.secret}")
	private String secret;
	
	@Autowired
	private WxUserMapper wxUserMapper;
	@Autowired
	private SysRoleMapper sysRoleMapper;
	@Autowired
    WxService wxService;

	/**
	 * 查询分页
	 */
	@Override
	public Map<String, Object> queryWxUser(Integer currentPage, Integer pageSize, String sorter) {
		Map<String, Object> paramMap = new HashMap<>();
		if (StringUtils.isNotBlank(sorter)) {
			String sort = sorter.substring(0, sorter.lastIndexOf('_'));
			String sequence = "ascend".equals(sorter.substring(sorter.lastIndexOf('_') + 1)) ? "ASC" : "DESC";
			paramMap.put("sort", sort);
			paramMap.put("sequence", sequence);
		} else {
			paramMap.put("sort", "createTime");
			paramMap.put("sequence", "DESC");
		}
		Page<Object> page = PageHelper.startPage(currentPage, pageSize);
		List<LinkedHashMap<String, Object>> resultList = wxUserMapper.queryWxUser(paramMap);

		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String roleData = sysRoleMapper.queryRoleData("wxuser", authentication.getName());
		String[] roleDataArray = roleData == null ? null : roleData.split(",");
		if (roleDataArray != null && roleDataArray.length > 0) {// 处理数据权限
			return PaginationBuilder.buildResult(CollectionUtils.convertFilterList(resultList, roleDataArray), page.getTotal(), currentPage, pageSize);
		} else {
			return PaginationBuilder.buildResult(resultList, page.getTotal(), currentPage, pageSize);
		}
	}

	/**
	 * 查询导出数据列表
	 */
	@Override
	public List<LinkedHashMap<String, Object>> queryWxUserForExcel(Map<String, Object> paramMap) {
		return wxUserMapper.queryWxUser(paramMap);
	}

	/**
	 * 新增
	 */
	@Override
	public void insertWxUser(WxUser wxUser) {
		wxUser.setId(sequenceGenerator.nextId());
		wxUserMapper.insertWxUser(wxUser);
		logger.info("已新增： {}", wxUser.getId());
	}

	/**
	 * 编辑
	 */
	@Override
	public void updateWxUser(WxUser wxUser) {
		wxUserMapper.updateWxUser(wxUser);
		logger.info("已编辑： {}", wxUser.getId());
	}

	/**
	 * 删除
	 */
	@Override
	public void deleteWxUser(Long[] id) {
		wxUserMapper.deleteWxUser(id);
	}

	@Override
	    public String getSessionId(String code) {
	
	        String url = "https://api.weixin.qq.com/sns/jscode2session?appid={0}&secret={1}&js_code={2}&grant_type=authorization_code";
	        String replaceUrl = url.replace("{0}", appid).replace("{1}", secret).replace("{2}", code);
	        String res = HttpUtil.get(replaceUrl);
	        logger.info("发送链接后获得的数据{}",res);
	        String s = UUID.randomUUID().toString();
	        return s;
	    }

	@Override
	public Result wxUserLogin(Auth auth) {
		try {
			String wxRes = wxService.wxDecrypt(auth.getEncryptedData(), auth.getSessionId(), auth.getIv());
			logger.info("信息："+wxRes);
			WxUser wxUser = JSON.parseObject(wxRes,WxUser.class);
			System.out.println(wxUser);
			//这里是做业务操作的，理论上需要查询数据库，看这个用户信息是否存在，存在直接返回登录态，
			// 若不存在即添加进数据库，做持久化。（表建好了，相关依赖也添加了，发现这是demo.就... 你懂的哈）
			// 根据自己需求 更改
			return Result.SUCCESS(wxUser);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Result.FAIL();
	}

}
