package com.powercode.osce.service.impl;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.powercode.admin.service.SysDictService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.powercode.osce.entity.Cases;
import com.powercode.admin.mapper.SysRoleMapper;
import com.powercode.osce.mapper.CasesMapper;
import com.powercode.osce.service.CasesService;
import com.powercode.component.commons.result.PaginationBuilder;
import com.powercode.component.commons.utils.CollectionUtils;
import com.powercode.component.commons.utils.SequenceGenerator;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

/**
 * 业务逻辑实现层
 */
@Service
@Transactional
public class CasesServiceImpl implements CasesService {

	private final Logger logger = LoggerFactory.getLogger(getClass());
	private static SequenceGenerator sequenceGenerator = new SequenceGenerator();

	@Autowired
	private CasesMapper casesMapper;
	@Autowired
	private SysRoleMapper sysRoleMapper;
	@Autowired
	private SysDictService sysDictService;

	/**
	 * 查询分页
	 */
	@Override
	public Map<String, Object> queryCases(Integer currentPage, Integer pageSize, String sorter) {
		Map<String, Object> paramMap = new HashMap<>();
		if (StringUtils.isNotBlank(sorter)) {
			String sort = sorter.substring(0, sorter.lastIndexOf('_'));
			String sequence = "ascend".equals(sorter.substring(sorter.lastIndexOf('_') + 1)) ? "ASC" : "DESC";
			paramMap.put("sort", sort);
			paramMap.put("sequence", sequence);
		} else {
			paramMap.put("sort", "createTime");
			paramMap.put("sequence", "DESC");
		}
		Page<Object> page = PageHelper.startPage(currentPage, pageSize);
		List<LinkedHashMap<String, Object>> resultList = casesMapper.queryCases(paramMap);

		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String roleData = sysRoleMapper.queryRoleData("cases", authentication.getName());
		String[] roleDataArray = roleData == null ? null : roleData.split(",");
		if (roleDataArray != null && roleDataArray.length > 0) {// 处理数据权限
			return PaginationBuilder.buildResult(CollectionUtils.convertFilterList(resultList, roleDataArray), page.getTotal(), currentPage, pageSize);
		} else {
			return PaginationBuilder.buildResult(resultList, page.getTotal(), currentPage, pageSize);
		}
	}

	/**
	 * 查询导出数据列表
	 */
	@Override
	public List<LinkedHashMap<String, Object>> queryCasesForExcel(Map<String, Object> paramMap) {
		return casesMapper.queryCases(paramMap);
	}

	/**
	 * 新增
	 */
	@Override
	public void insertCases(Cases cases) {
		cases.setId(sequenceGenerator.nextId());
		casesMapper.insertCases(cases);
		logger.info("已新增： {}", cases.getId());
	}

	/**
	 * 编辑
	 */
	@Override
	public void updateCases(Cases cases) {
		casesMapper.updateCases(cases);
		logger.info("已编辑： {}", cases.getId());
	}

	/**
	 * 删除
	 */
	@Override
	public void deleteCases(Long[] id) {
		casesMapper.deleteCases(id);
	}

	/**
	 * 查询机构类型的下拉框数据
	 */
	@Override
	public LinkedHashMap<String, Object> queryExamType() {
		LinkedHashMap<String, Object> resultMap = new LinkedHashMap<>();
		List<LinkedHashMap<String, Object>> resultList = sysDictService.queryDictByDictType("exam");
		resultMap.put("list", resultList);
		return resultMap;
	}

}


