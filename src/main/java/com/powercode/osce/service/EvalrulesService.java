package com.powercode.osce.service;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.powercode.osce.entity.Evalrules;

/**
 * 业务逻辑接口层
 */
public interface EvalrulesService {

	/**
	 * 查询分页
	 * 
	 * @param currentPage   当前页数
	 * @param pageSize      每页记录数
	 * @param sorter        排序
	 * @return
	 */
	Map<String, Object> queryEvalrules(Integer currentPage, Integer pageSize, String sorter);

	/**
	 * 查询导出数据列表
	 * 
	 * @param paramMap 参数Map
	 * @return
	 */
	List<LinkedHashMap<String, Object>> queryEvalrulesForExcel(Map<String, Object> paramMap);

	/**
	 * 新增
	 * 
	 * @param evalrules 对象
	 */
	void insertEvalrules(Evalrules evalrules);

	/**
	 * 编辑
	 * 
	 * @param evalrules 对象
	 */
	void updateEvalrules(Evalrules evalrules);

	/**
	 * 删除
	 * 
	 * @param id ID
	 */
	void deleteEvalrules(Long[] id);

}
