package com.powercode.osce.service;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.powercode.osce.entity.ExamAudio;

/**
 * 业务逻辑接口层
 */
public interface ExamAudioService {

	/**
	 * 查询分页
	 * 
	 * @param currentPage   当前页数
	 * @param pageSize      每页记录数
	 * @param sorter        排序
	 * @return
	 */
	Map<String, Object> queryExamAudio(Integer currentPage, Integer pageSize, String sorter);

	/**
	 * 查询导出数据列表
	 * 
	 * @param paramMap 参数Map
	 * @return
	 */
	List<LinkedHashMap<String, Object>> queryExamAudioForExcel(Map<String, Object> paramMap);

	/**
	 * 新增
	 * 
	 * @param examAudio 对象
	 */
	void insertExamAudio(ExamAudio examAudio);

	/**
	 * 编辑
	 * 
	 * @param examAudio 对象
	 */
	void updateExamAudio(ExamAudio examAudio);

	/**
	 * 删除
	 * 
	 * @param id ID
	 */
	void deleteExamAudio(Long[] id);

}
