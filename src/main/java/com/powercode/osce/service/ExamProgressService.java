package com.powercode.osce.service;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.powercode.osce.entity.ExamProgress;

/**
 * 业务逻辑接口层
 */
public interface ExamProgressService {

	/**
	 * 查询分页
	 * 
	 * @param currentPage   当前页数
	 * @param pageSize      每页记录数
	 * @param sorter        排序
	 * @return
	 */
	Map<String, Object> queryExamProgress(Integer currentPage, Integer pageSize, String sorter);

	/**
	 * 查询导出数据列表
	 * 
	 * @param paramMap 参数Map
	 * @return
	 */
	List<LinkedHashMap<String, Object>> queryExamProgressForExcel(Map<String, Object> paramMap);

	/**
	 * 新增
	 * 
	 * @param examProgress 对象
	 */
	void insertExamProgress(ExamProgress examProgress);

	/**
	 * 编辑
	 * 
	 * @param examProgress 对象
	 */
	void updateExamProgress(ExamProgress examProgress);

	/**
	 * 删除
	 * 
	 * @param id ID
	 */
	void deleteExamProgress(Long[] id);

}
