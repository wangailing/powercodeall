package com.powercode.osce.mapper;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.powercode.osce.entity.Cases;

/**
 * 数据持久接口层
 */
public interface CasesMapper {

	/**
	 * 查询分页或导出数据
	 * 
	 * @param paramMap 参数Map
	 * @return
	 */
	List<LinkedHashMap<String, Object>> queryCases(Map<String, Object> paramMap);

	/**
	 * 新增
	 * 
	 * @param cases 对象
	 * @return
	 */
	int insertCases(Cases cases);

	/**
	 * 编辑
	 * 
	 * @param cases 对象
	 * @return
	 */
	int updateCases(Cases cases);

	/**
	 * 删除
	 * 
	 * @param id ID
	 * @return
	 */
	int deleteCases(Long[] id);

}
