package com.powercode.osce.mapper;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.powercode.osce.entity.Evalrules;

/**
 * 数据持久接口层
 */
public interface EvalrulesMapper {

	/**
	 * 查询分页或导出数据
	 * 
	 * @param paramMap 参数Map
	 * @return
	 */
	List<LinkedHashMap<String, Object>> queryEvalrules(Map<String, Object> paramMap);

	/**
	 * 新增
	 * 
	 * @param evalrules 对象
	 * @return
	 */
	int insertEvalrules(Evalrules evalrules);

	/**
	 * 编辑
	 * 
	 * @param evalrules 对象
	 * @return
	 */
	int updateEvalrules(Evalrules evalrules);

	/**
	 * 删除
	 * 
	 * @param id ID
	 * @return
	 */
	int deleteEvalrules(Long[] id);

}
