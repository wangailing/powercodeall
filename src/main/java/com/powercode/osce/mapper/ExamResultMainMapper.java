package com.powercode.osce.mapper;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.powercode.osce.entity.ExamResultMain;

/**
 * 数据持久接口层
 */
public interface ExamResultMainMapper {

	/**
	 * 查询分页或导出数据
	 * 
	 * @param paramMap 参数Map
	 * @return
	 */
	List<LinkedHashMap<String, Object>> queryExamResultMain(Map<String, Object> paramMap);

	/**
	 * 新增
	 * 
	 * @param examResultMain 对象
	 * @return
	 */
	int insertExamResultMain(ExamResultMain examResultMain);

	/**
	 * 编辑
	 * 
	 * @param examResultMain 对象
	 * @return
	 */
	int updateExamResultMain(ExamResultMain examResultMain);

	/**
	 * 删除
	 * 
	 * @param id ID
	 * @return
	 */
	int deleteExamResultMain(Long[] id);

}
