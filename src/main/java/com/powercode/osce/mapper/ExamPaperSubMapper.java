package com.powercode.osce.mapper;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.powercode.osce.entity.ExamPaperSub;

/**
 * 数据持久接口层
 */
public interface ExamPaperSubMapper {

	/**
	 * 查询分页或导出数据
	 * 
	 * @param paramMap 参数Map
	 * @return
	 */
	List<LinkedHashMap<String, Object>> queryExamPaperSub(Map<String, Object> paramMap);

	/**
	 * 新增
	 * 
	 * @param examPaperSub 对象
	 * @return
	 */
	int insertExamPaperSub(ExamPaperSub examPaperSub);

	/**
	 * 编辑
	 * 
	 * @param examPaperSub 对象
	 * @return
	 */
	int updateExamPaperSub(ExamPaperSub examPaperSub);

	/**
	 * 删除
	 * 
	 * @param id ID
	 * @return
	 */
	int deleteExamPaperSub(Long[] id);

}
