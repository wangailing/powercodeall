package com.powercode.osce.mapper;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.powercode.osce.entity.ExamStation;

/**
 * 数据持久接口层
 */
public interface ExamStationMapper {

	/**
	 * 查询分页或导出数据
	 * 
	 * @param paramMap 参数Map
	 * @return
	 */
	List<LinkedHashMap<String, Object>> queryExamStation(Map<String, Object> paramMap);

	/**
	 * 新增
	 * 
	 * @param examStation 对象
	 * @return
	 */
	int insertExamStation(ExamStation examStation);

	/**
	 * 编辑
	 * 
	 * @param examStation 对象
	 * @return
	 */
	int updateExamStation(ExamStation examStation);

	/**
	 * 删除
	 * 
	 * @param id ID
	 * @return
	 */
	int deleteExamStation(Long[] id);

}
