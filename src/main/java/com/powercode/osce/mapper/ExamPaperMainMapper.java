package com.powercode.osce.mapper;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.powercode.osce.entity.ExamPaperMain;

/**
 * 数据持久接口层
 */
public interface ExamPaperMainMapper {

	/**
	 * 查询分页或导出数据
	 * 
	 * @param paramMap 参数Map
	 * @return
	 */
	List<LinkedHashMap<String, Object>> queryExamPaperMain(Map<String, Object> paramMap);

	/**
	 * 新增
	 * 
	 * @param examPaperMain 对象
	 * @return
	 */
	int insertExamPaperMain(ExamPaperMain examPaperMain);

	/**
	 * 编辑
	 * 
	 * @param examPaperMain 对象
	 * @return
	 */
	int updateExamPaperMain(ExamPaperMain examPaperMain);

	/**
	 * 删除
	 * 
	 * @param id ID
	 * @return
	 */
	int deleteExamPaperMain(Long[] id);

}
