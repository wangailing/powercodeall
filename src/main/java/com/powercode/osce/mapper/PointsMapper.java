package com.powercode.osce.mapper;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.powercode.osce.entity.Points;

/**
 * 数据持久接口层
 */
public interface PointsMapper {

	/**
	 * 查询分页或导出数据
	 * 
	 * @param paramMap 参数Map
	 * @return
	 */
	List<LinkedHashMap<String, Object>> queryPoints(Map<String, Object> paramMap);

	/**
	 * 新增
	 * 
	 * @param points 对象
	 * @return
	 */
	int insertPoints(Points points);

	/**
	 * 编辑
	 * 
	 * @param points 对象
	 * @return
	 */
	int updatePoints(Points points);

	/**
	 * 删除
	 * 
	 * @param id ID
	 * @return
	 */
	int deletePoints(Long[] id);

}
