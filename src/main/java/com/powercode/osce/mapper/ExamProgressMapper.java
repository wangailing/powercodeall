package com.powercode.osce.mapper;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.powercode.osce.entity.ExamProgress;

/**
 * 数据持久接口层
 */
public interface ExamProgressMapper {

	/**
	 * 查询分页或导出数据
	 * 
	 * @param paramMap 参数Map
	 * @return
	 */
	List<LinkedHashMap<String, Object>> queryExamProgress(Map<String, Object> paramMap);

	/**
	 * 新增
	 * 
	 * @param examProgress 对象
	 * @return
	 */
	int insertExamProgress(ExamProgress examProgress);

	/**
	 * 编辑
	 * 
	 * @param examProgress 对象
	 * @return
	 */
	int updateExamProgress(ExamProgress examProgress);

	/**
	 * 删除
	 * 
	 * @param id ID
	 * @return
	 */
	int deleteExamProgress(Long[] id);

}
