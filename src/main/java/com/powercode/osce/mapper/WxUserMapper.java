package com.powercode.osce.mapper;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.powercode.osce.entity.WxUser;

/**
 * 数据持久接口层
 */
public interface WxUserMapper {

	/**
	 * 查询分页或导出数据
	 * 
	 * @param paramMap 参数Map
	 * @return
	 */
	List<LinkedHashMap<String, Object>> queryWxUser(Map<String, Object> paramMap);

	/**
	 * 新增
	 * 
	 * @param wxUser 对象
	 * @return
	 */
	int insertWxUser(WxUser wxUser);

	/**
	 * 编辑
	 * 
	 * @param wxUser 对象
	 * @return
	 */
	int updateWxUser(WxUser wxUser);

	/**
	 * 删除
	 * 
	 * @param id ID
	 * @return
	 */
	int deleteWxUser(Long[] id);

}
