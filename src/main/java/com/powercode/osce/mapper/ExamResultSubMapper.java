package com.powercode.osce.mapper;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.powercode.osce.entity.ExamResultSub;

/**
 * 数据持久接口层
 */
public interface ExamResultSubMapper {

	/**
	 * 查询分页或导出数据
	 * 
	 * @param paramMap 参数Map
	 * @return
	 */
	List<LinkedHashMap<String, Object>> queryExamResultSub(Map<String, Object> paramMap);

	/**
	 * 新增
	 * 
	 * @param examResultSub 对象
	 * @return
	 */
	int insertExamResultSub(ExamResultSub examResultSub);

	/**
	 * 编辑
	 * 
	 * @param examResultSub 对象
	 * @return
	 */
	int updateExamResultSub(ExamResultSub examResultSub);

	/**
	 * 删除
	 * 
	 * @param id ID
	 * @return
	 */
	int deleteExamResultSub(Long[] id);

}
