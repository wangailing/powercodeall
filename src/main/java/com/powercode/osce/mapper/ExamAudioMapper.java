package com.powercode.osce.mapper;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.powercode.osce.entity.ExamAudio;

/**
 * 数据持久接口层
 */
public interface ExamAudioMapper {

	/**
	 * 查询分页或导出数据
	 * 
	 * @param paramMap 参数Map
	 * @return
	 */
	List<LinkedHashMap<String, Object>> queryExamAudio(Map<String, Object> paramMap);

	/**
	 * 新增
	 * 
	 * @param examAudio 对象
	 * @return
	 */
	int insertExamAudio(ExamAudio examAudio);

	/**
	 * 编辑
	 * 
	 * @param examAudio 对象
	 * @return
	 */
	int updateExamAudio(ExamAudio examAudio);

	/**
	 * 删除
	 * 
	 * @param id ID
	 * @return
	 */
	int deleteExamAudio(Long[] id);

}
