package com.powercode.osce.controller;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.powercode.osce.entity.ExamResultMain;
import com.powercode.osce.service.ExamResultMainService;
import com.powercode.osce.vo.ExamResultMainVO;
import com.powercode.component.commons.result.ActionResult;
import com.powercode.component.commons.result.ListResult;
import com.powercode.component.commons.result.ResultBuilder;
import com.powercode.component.commons.utils.ExcelUtils;
import com.powercode.component.commons.validator.InsertValidator;
import com.powercode.component.commons.validator.UpdateValidator;

/**
 * 控制层
 */
@RestController
@RequestMapping("/examresultmain")
public class ExamResultMainController {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private ExamResultMainService examResultMainService;

	/**
	 * 查询分页
	 * 
	 * @param examResultMainVO 前端参数
	 * @return
	 */
	@GetMapping(path = "/queryExamResultMain")
	public ListResult<Object> queryExamResultMain(ExamResultMainVO examResultMainVO) {
		Map<String, Object> data = examResultMainService.queryExamResultMain(examResultMainVO.getCurrentPage(), examResultMainVO.getPageSize(), examResultMainVO.getSorter());
		return ResultBuilder.buildListSuccess(data);
	}

	/**
	 * 新增
	 * 
	 * @param examResultMain 对象
	 * @return
	 */
	@PostMapping(path = "/addExamResultMain")
	public ActionResult addExamResultMain(@Validated(InsertValidator.class) @RequestBody ExamResultMain examResultMain) {
		examResultMainService.insertExamResultMain(examResultMain);
		return ResultBuilder.buildActionSuccess();
	}

	/**
	 * 编辑
	 * 
	 * @param examResultMain 对象
	 * @return
	 */
	@PutMapping(path = "/updateExamResultMain")
	public ActionResult updateExamResultMain(@Validated(UpdateValidator.class) @RequestBody ExamResultMain examResultMain) {
		examResultMainService.updateExamResultMain(examResultMain);
		return ResultBuilder.buildActionSuccess();
	}

	/**
	 * 删除
	 * 
	 * @param id ID
	 * @return
	 */
	@PostMapping(path = "/deleteExamResultMain")
	public ActionResult deleteExamResultMain(@RequestParam(name = "id", required = true) Long[] id) {
		examResultMainService.deleteExamResultMain(id);
		return ResultBuilder.buildActionSuccess();
	}

	/**
	 * 根据查询条件导出
	 * 
	 * @param response 响应对象
	 * @param paramMap 参数Map
	 */
	@PostMapping(path = "/exportExamResultMain")
	public void exportExamResultMain(HttpServletResponse response, @RequestParam Map<String, Object> paramMap) {
		try {
			List<String> headList = Arrays.asList( "", "", "", "", "", "", "", "");
			List<LinkedHashMap<String, Object>> dataList = examResultMainService.queryExamResultMainForExcel(paramMap);
			ExcelUtils.exportExcel(headList, dataList, "导出信息", response);
		} catch (Exception e) {
			logger.warn(e.toString());
		}
	}

}
