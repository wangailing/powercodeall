package com.powercode.osce.controller;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.powercode.osce.entity.Evalrules;
import com.powercode.osce.service.EvalrulesService;
import com.powercode.osce.vo.EvalrulesVO;
import com.powercode.component.commons.result.ActionResult;
import com.powercode.component.commons.result.ListResult;
import com.powercode.component.commons.result.ResultBuilder;
import com.powercode.component.commons.utils.ExcelUtils;
import com.powercode.component.commons.validator.InsertValidator;
import com.powercode.component.commons.validator.UpdateValidator;

/**
 * 控制层
 */
@RestController
@RequestMapping("/evalrules")
public class EvalrulesController {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private EvalrulesService evalrulesService;

	/**
	 * 查询分页
	 * 
	 * @param evalrulesVO 前端参数
	 * @return
	 */
	@GetMapping(path = "/queryEvalrules")
	public ListResult<Object> queryEvalrules(EvalrulesVO evalrulesVO) {
		Map<String, Object> data = evalrulesService.queryEvalrules(evalrulesVO.getCurrentPage(), evalrulesVO.getPageSize(), evalrulesVO.getSorter());
		return ResultBuilder.buildListSuccess(data);
	}

	/**
	 * 新增
	 * 
	 * @param evalrules 对象
	 * @return
	 */
	@PostMapping(path = "/addEvalrules")
	public ActionResult addEvalrules(@Validated(InsertValidator.class) @RequestBody Evalrules evalrules) {
		evalrulesService.insertEvalrules(evalrules);
		return ResultBuilder.buildActionSuccess();
	}

	/**
	 * 编辑
	 * 
	 * @param evalrules 对象
	 * @return
	 */
	@PutMapping(path = "/updateEvalrules")
	public ActionResult updateEvalrules(@Validated(UpdateValidator.class) @RequestBody Evalrules evalrules) {
		evalrulesService.updateEvalrules(evalrules);
		return ResultBuilder.buildActionSuccess();
	}

	/**
	 * 删除
	 * 
	 * @param id ID
	 * @return
	 */
	@PostMapping(path = "/deleteEvalrules")
	public ActionResult deleteEvalrules(@RequestParam(name = "id", required = true) Long[] id) {
		evalrulesService.deleteEvalrules(id);
		return ResultBuilder.buildActionSuccess();
	}

	/**
	 * 根据查询条件导出
	 * 
	 * @param response 响应对象
	 * @param paramMap 参数Map
	 */
	@PostMapping(path = "/exportEvalrules")
	public void exportEvalrules(HttpServletResponse response, @RequestParam Map<String, Object> paramMap) {
		try {
			List<String> headList = Arrays.asList( "", "", "", "");
			List<LinkedHashMap<String, Object>> dataList = evalrulesService.queryEvalrulesForExcel(paramMap);
			ExcelUtils.exportExcel(headList, dataList, "导出信息", response);
		} catch (Exception e) {
			logger.warn(e.toString());
		}
	}

}
