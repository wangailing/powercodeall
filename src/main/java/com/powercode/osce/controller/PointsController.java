package com.powercode.osce.controller;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.powercode.osce.entity.Points;
import com.powercode.osce.service.PointsService;
import com.powercode.osce.vo.PointsVO;
import com.powercode.component.commons.result.ActionResult;
import com.powercode.component.commons.result.ListResult;
import com.powercode.component.commons.result.ResultBuilder;
import com.powercode.component.commons.utils.ExcelUtils;
import com.powercode.component.commons.validator.InsertValidator;
import com.powercode.component.commons.validator.UpdateValidator;

/**
 * 控制层
 */
@RestController
@RequestMapping("/points")
public class PointsController {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private PointsService pointsService;

	/**
	 * 查询分页
	 * 
	 * @param pointsVO 前端参数
	 * @return
	 */
	@GetMapping(path = "/queryPoints")
	public ListResult<Object> queryPoints(PointsVO pointsVO) {
		Map<String, Object> data = pointsService.queryPoints(pointsVO.getCurrentPage(), pointsVO.getPageSize(), pointsVO.getSorter());
		return ResultBuilder.buildListSuccess(data);
	}

	/**
	 * 新增
	 * 
	 * @param points 对象
	 * @return
	 */
	@PostMapping(path = "/addPoints")
	public ActionResult addPoints(@Validated(InsertValidator.class) @RequestBody Points points) {
		pointsService.insertPoints(points);
		return ResultBuilder.buildActionSuccess();
	}

	/**
	 * 编辑
	 * 
	 * @param points 对象
	 * @return
	 */
	@PutMapping(path = "/updatePoints")
	public ActionResult updatePoints(@Validated(UpdateValidator.class) @RequestBody Points points) {
		pointsService.updatePoints(points);
		return ResultBuilder.buildActionSuccess();
	}

	/**
	 * 删除
	 * 
	 * @param id ID
	 * @return
	 */
	@PostMapping(path = "/deletePoints")
	public ActionResult deletePoints(@RequestParam(name = "id", required = true) Long[] id) {
		pointsService.deletePoints(id);
		return ResultBuilder.buildActionSuccess();
	}

	/**
	 * 根据查询条件导出
	 * 
	 * @param response 响应对象
	 * @param paramMap 参数Map
	 */
	@PostMapping(path = "/exportPoints")
	public void exportPoints(HttpServletResponse response, @RequestParam Map<String, Object> paramMap) {
		try {
			List<String> headList = Arrays.asList( "", "", "", "", "");
			List<LinkedHashMap<String, Object>> dataList = pointsService.queryPointsForExcel(paramMap);
			ExcelUtils.exportExcel(headList, dataList, "导出信息", response);
		} catch (Exception e) {
			logger.warn(e.toString());
		}
	}

}
