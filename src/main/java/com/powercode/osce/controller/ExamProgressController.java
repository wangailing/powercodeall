package com.powercode.osce.controller;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.powercode.osce.entity.ExamProgress;
import com.powercode.osce.service.ExamProgressService;
import com.powercode.osce.vo.ExamProgressVO;
import com.powercode.component.commons.result.ActionResult;
import com.powercode.component.commons.result.ListResult;
import com.powercode.component.commons.result.ResultBuilder;
import com.powercode.component.commons.utils.ExcelUtils;
import com.powercode.component.commons.validator.InsertValidator;
import com.powercode.component.commons.validator.UpdateValidator;

/**
 * 控制层
 */
@RestController
@RequestMapping("/examprogress")
public class ExamProgressController {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private ExamProgressService examProgressService;

	/**
	 * 查询分页
	 * 
	 * @param examProgressVO 前端参数
	 * @return
	 */
	@GetMapping(path = "/queryExamProgress")
	public ListResult<Object> queryExamProgress(ExamProgressVO examProgressVO) {
		Map<String, Object> data = examProgressService.queryExamProgress(examProgressVO.getCurrentPage(), examProgressVO.getPageSize(), examProgressVO.getSorter());
		return ResultBuilder.buildListSuccess(data);
	}

	/**
	 * 新增
	 * 
	 * @param examProgress 对象
	 * @return
	 */
	@PostMapping(path = "/addExamProgress")
	public ActionResult addExamProgress(@Validated(InsertValidator.class) @RequestBody ExamProgress examProgress) {
		examProgressService.insertExamProgress(examProgress);
		return ResultBuilder.buildActionSuccess();
	}

	/**
	 * 编辑
	 * 
	 * @param examProgress 对象
	 * @return
	 */
	@PutMapping(path = "/updateExamProgress")
	public ActionResult updateExamProgress(@Validated(UpdateValidator.class) @RequestBody ExamProgress examProgress) {
		examProgressService.updateExamProgress(examProgress);
		return ResultBuilder.buildActionSuccess();
	}

	/**
	 * 删除
	 * 
	 * @param id ID
	 * @return
	 */
	@PostMapping(path = "/deleteExamProgress")
	public ActionResult deleteExamProgress(@RequestParam(name = "id", required = true) Long[] id) {
		examProgressService.deleteExamProgress(id);
		return ResultBuilder.buildActionSuccess();
	}

	/**
	 * 根据查询条件导出
	 * 
	 * @param response 响应对象
	 * @param paramMap 参数Map
	 */
	@PostMapping(path = "/exportExamProgress")
	public void exportExamProgress(HttpServletResponse response, @RequestParam Map<String, Object> paramMap) {
		try {
			List<String> headList = Arrays.asList( "", "", "", "", "", "");
			List<LinkedHashMap<String, Object>> dataList = examProgressService.queryExamProgressForExcel(paramMap);
			ExcelUtils.exportExcel(headList, dataList, "导出信息", response);
		} catch (Exception e) {
			logger.warn(e.toString());
		}
	}

}
