package com.powercode.osce.controller;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import com.powercode.component.commons.wxcommons.Auth;
import com.powercode.component.commons.wxcommons.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.powercode.osce.entity.WxUser;
import com.powercode.osce.service.WxUserService;
import com.powercode.osce.vo.WxUserVO;
import com.powercode.component.commons.result.ActionResult;
import com.powercode.component.commons.result.ListResult;
import com.powercode.component.commons.result.ResultBuilder;
import com.powercode.component.commons.utils.ExcelUtils;
import com.powercode.component.commons.validator.InsertValidator;
import com.powercode.component.commons.validator.UpdateValidator;

/**
 * 控制层
 */
@RestController
@RequestMapping("/wxuser")
public class WxUserController {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private WxUserService wxUserService;

	/**
	 * 查询分页
	 * 
	 * @param wxUserVO 前端参数
	 * @return
	 */
	@GetMapping(path = "/queryWxUser")
	public ListResult<Object> queryWxUser(WxUserVO wxUserVO) {
		Map<String, Object> data = wxUserService.queryWxUser(wxUserVO.getCurrentPage(), wxUserVO.getPageSize(), wxUserVO.getSorter());
		return ResultBuilder.buildListSuccess(data);
	}

	/**
	 * 新增
	 * 
	 * @param wxUser 对象
	 * @return
	 */
	@PostMapping(path = "/addWxUser")
	public ActionResult addWxUser(@Validated(InsertValidator.class) @RequestBody WxUser wxUser) {
		wxUserService.insertWxUser(wxUser);
		return ResultBuilder.buildActionSuccess();
	}

	/**
	 * 编辑
	 * 
	 * @param wxUser 对象
	 * @return
	 */
	@PutMapping(path = "/updateWxUser")
	public ActionResult updateWxUser(@Validated(UpdateValidator.class) @RequestBody WxUser wxUser) {
		wxUserService.updateWxUser(wxUser);
		return ResultBuilder.buildActionSuccess();
	}

	/**
	 * 删除
	 * 
	 * @param id ID
	 * @return
	 */
	@PostMapping(path = "/deleteWxUser")
	public ActionResult deleteWxUser(@RequestParam(name = "id", required = true) Long[] id) {
		wxUserService.deleteWxUser(id);
		return ResultBuilder.buildActionSuccess();
	}

	/**
	 * 根据查询条件导出
	 * 
	 * @param response 响应对象
	 * @param paramMap 参数Map
	 */
	@PostMapping(path = "/exportWxUser")
	public void exportWxUser(HttpServletResponse response, @RequestParam Map<String, Object> paramMap) {
		try {
			List<String> headList = Arrays.asList( "", "", "", "", "", "", "", "", "", "", "");
			List<LinkedHashMap<String, Object>> dataList = wxUserService.queryWxUserForExcel(paramMap);
			ExcelUtils.exportExcel(headList, dataList, "导出信息", response);
		} catch (Exception e) {
			logger.warn(e.toString());
		}
	}

	/**
	 * sessionID
	 *
	 * @param code 对象
	 * @return
	 */
	@PostMapping(path = "/sessionId/{code}")
	public String getSessionId(@PathVariable("code") String code) {
		return wxUserService.getSessionId(code);
	}



	/**
	 * login
	 *
	 * @param auth 对象
	 * @return
	 */
	@PostMapping(path = "/authLogin")
	public Result wxUserLogin(@RequestBody Auth auth) {
		Result result = wxUserService.wxUserLogin(auth);
		return result;
	}

}
