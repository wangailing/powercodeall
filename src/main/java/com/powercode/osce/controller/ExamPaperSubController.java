package com.powercode.osce.controller;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.powercode.osce.entity.ExamPaperSub;
import com.powercode.osce.service.ExamPaperSubService;
import com.powercode.osce.vo.ExamPaperSubVO;
import com.powercode.component.commons.result.ActionResult;
import com.powercode.component.commons.result.ListResult;
import com.powercode.component.commons.result.ResultBuilder;
import com.powercode.component.commons.utils.ExcelUtils;
import com.powercode.component.commons.validator.InsertValidator;
import com.powercode.component.commons.validator.UpdateValidator;

/**
 * 控制层
 */
@RestController
@RequestMapping("/exampapersub")
public class ExamPaperSubController {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private ExamPaperSubService examPaperSubService;

	/**
	 * 查询分页
	 * 
	 * @param examPaperSubVO 前端参数
	 * @return
	 */
	@GetMapping(path = "/queryExamPaperSub")
	public ListResult<Object> queryExamPaperSub(ExamPaperSubVO examPaperSubVO) {
		Map<String, Object> data = examPaperSubService.queryExamPaperSub(examPaperSubVO.getCurrentPage(), examPaperSubVO.getPageSize(), examPaperSubVO.getSorter());
		return ResultBuilder.buildListSuccess(data);
	}

	/**
	 * 新增
	 * 
	 * @param examPaperSub 对象
	 * @return
	 */
	@PostMapping(path = "/addExamPaperSub")
	public ActionResult addExamPaperSub(@Validated(InsertValidator.class) @RequestBody ExamPaperSub examPaperSub) {
		examPaperSubService.insertExamPaperSub(examPaperSub);
		return ResultBuilder.buildActionSuccess();
	}

	/**
	 * 编辑
	 * 
	 * @param examPaperSub 对象
	 * @return
	 */
	@PutMapping(path = "/updateExamPaperSub")
	public ActionResult updateExamPaperSub(@Validated(UpdateValidator.class) @RequestBody ExamPaperSub examPaperSub) {
		examPaperSubService.updateExamPaperSub(examPaperSub);
		return ResultBuilder.buildActionSuccess();
	}

	/**
	 * 删除
	 * 
	 * @param id ID
	 * @return
	 */
	@PostMapping(path = "/deleteExamPaperSub")
	public ActionResult deleteExamPaperSub(@RequestParam(name = "id", required = true) Long[] id) {
		examPaperSubService.deleteExamPaperSub(id);
		return ResultBuilder.buildActionSuccess();
	}

	/**
	 * 根据查询条件导出
	 * 
	 * @param response 响应对象
	 * @param paramMap 参数Map
	 */
	@PostMapping(path = "/exportExamPaperSub")
	public void exportExamPaperSub(HttpServletResponse response, @RequestParam Map<String, Object> paramMap) {
		try {
			List<String> headList = Arrays.asList( "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
			List<LinkedHashMap<String, Object>> dataList = examPaperSubService.queryExamPaperSubForExcel(paramMap);
			ExcelUtils.exportExcel(headList, dataList, "导出信息", response);
		} catch (Exception e) {
			logger.warn(e.toString());
		}
	}

}
