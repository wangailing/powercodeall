package com.powercode.osce.controller;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import com.powercode.admin.mapper.SysDictMapper;
import com.powercode.admin.mapper.SysUserMapper;
import com.powercode.admin.service.SysOrgService;
import com.powercode.auth.mapper.AuthMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.powercode.osce.entity.Cases;
import com.powercode.osce.service.CasesService;
import com.powercode.osce.vo.CasesVO;
import com.powercode.component.commons.result.ActionResult;
import com.powercode.component.commons.result.ListResult;
import com.powercode.component.commons.result.ResultBuilder;
import com.powercode.component.commons.utils.ExcelUtils;
import com.powercode.component.commons.validator.InsertValidator;
import com.powercode.component.commons.validator.UpdateValidator;

/**
 * 控制层
 */
@RestController
@RequestMapping("/cases")
public class CasesController {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private CasesService casesService;
	@Autowired
	private AuthMapper authMapper;
	@Autowired
	private SysUserMapper sysUserMapper;
	@Autowired
	private SysDictMapper sysDictMapper;
	/**
	 * 查询分页
	 * 
	 * @param casesVO 前端参数
	 * @return
	 */
	@GetMapping(path = "/queryCases")
	public ListResult<Object> queryCases(CasesVO casesVO) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		User user = (User)authentication.getPrincipal();
		String uName = user.getUsername();
		String[] userName = new String[] {uName};
		List<Long> id = sysUserMapper.querySysUserId(userName);
		String user_id = String.valueOf(id.get(0));
		Map<String, Object> data = casesService.queryCases(casesVO.getCurrentPage(), casesVO.getPageSize(), casesVO.getSorter());
		data.put("user_id",user_id);
		return ResultBuilder.buildListSuccess(data);
	}

	/**
	 * 新增
	 * 
	 * @param cases 对象
	 * @return
	 */
	@PostMapping(path = "/addCases")
	public ActionResult addCases(@Validated(InsertValidator.class) @RequestBody Cases cases) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		Object obj = authentication.getPrincipal();
		casesService.insertCases(cases);
		return ResultBuilder.buildActionSuccess();
	}

	/**
	 * 编辑
	 * 
	 * @param cases 对象
	 * @return
	 */
	@PutMapping(path = "/updateCases")
	public ActionResult updateCases(@Validated(UpdateValidator.class) @RequestBody Cases cases) {

		//Map map = authMapper.getSysUserByUsername(userName);
		casesService.updateCases(cases);
		return ResultBuilder.buildActionSuccess();
	}

	/**
	 * 删除
	 * 
	 * @param id ID
	 * @return
	 */
	@PostMapping(path = "/deleteCases")
	public ActionResult deleteCases(@RequestParam(name = "id", required = true) Long[] id) {
		casesService.deleteCases(id);
		return ResultBuilder.buildActionSuccess();
	}

	/**
	 * 根据查询条件导出
	 * 
	 * @param response 响应对象
	 * @param paramMap 参数Map
	 */
	@PostMapping(path = "/exportCases")
	public void exportCases(HttpServletResponse response, @RequestParam Map<String, Object> paramMap) {
		try {
			List<String> headList = Arrays.asList( "", "", "", "", "", "", "", "", "", "", "", "", "");
			List<LinkedHashMap<String, Object>> dataList = casesService.queryCasesForExcel(paramMap);
			ExcelUtils.exportExcel(headList, dataList, "导出信息", response);
		} catch (Exception e) {
			logger.warn(e.toString());
		}
	}

	/**
	 * 查询机构类型的下拉框数据
	 *
	 * @return
	 */
	@GetMapping(path = "/queryExamAssitantRoleType")
	public ListResult<Object> queryExamAssitantRoleType() {
		List<LinkedHashMap<String, Object>> data = sysDictMapper.queryDictByDictType("osce_examassitant_role");
		return ResultBuilder.buildListSuccess(data);
	}


	/**
	 * 查询机构类型的下拉框数据
	 *
	 * @return
	 */
	@GetMapping(path = "/queryExamObjectType")
	public ListResult<Object> queryExamObjectType() {
		List<LinkedHashMap<String, Object>> data = sysDictMapper.queryDictByDictType("osce_examobject_type");
		return ResultBuilder.buildListSuccess(data);
	}

}
