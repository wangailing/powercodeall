package com.powercode.osce.controller;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.powercode.osce.entity.ExamResultSub;
import com.powercode.osce.service.ExamResultSubService;
import com.powercode.osce.vo.ExamResultSubVO;
import com.powercode.component.commons.result.ActionResult;
import com.powercode.component.commons.result.ListResult;
import com.powercode.component.commons.result.ResultBuilder;
import com.powercode.component.commons.utils.ExcelUtils;
import com.powercode.component.commons.validator.InsertValidator;
import com.powercode.component.commons.validator.UpdateValidator;

/**
 * 控制层
 */
@RestController
@RequestMapping("/examresultsub")
public class ExamResultSubController {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private ExamResultSubService examResultSubService;

	/**
	 * 查询分页
	 * 
	 * @param examResultSubVO 前端参数
	 * @return
	 */
	@GetMapping(path = "/queryExamResultSub")
	public ListResult<Object> queryExamResultSub(ExamResultSubVO examResultSubVO) {
		Map<String, Object> data = examResultSubService.queryExamResultSub(examResultSubVO.getCurrentPage(), examResultSubVO.getPageSize(), examResultSubVO.getSorter());
		return ResultBuilder.buildListSuccess(data);
	}

	/**
	 * 新增
	 * 
	 * @param examResultSub 对象
	 * @return
	 */
	@PostMapping(path = "/addExamResultSub")
	public ActionResult addExamResultSub(@Validated(InsertValidator.class) @RequestBody ExamResultSub examResultSub) {
		examResultSubService.insertExamResultSub(examResultSub);
		return ResultBuilder.buildActionSuccess();
	}

	/**
	 * 编辑
	 * 
	 * @param examResultSub 对象
	 * @return
	 */
	@PutMapping(path = "/updateExamResultSub")
	public ActionResult updateExamResultSub(@Validated(UpdateValidator.class) @RequestBody ExamResultSub examResultSub) {
		examResultSubService.updateExamResultSub(examResultSub);
		return ResultBuilder.buildActionSuccess();
	}

	/**
	 * 删除
	 * 
	 * @param id ID
	 * @return
	 */
	@PostMapping(path = "/deleteExamResultSub")
	public ActionResult deleteExamResultSub(@RequestParam(name = "id", required = true) Long[] id) {
		examResultSubService.deleteExamResultSub(id);
		return ResultBuilder.buildActionSuccess();
	}

	/**
	 * 根据查询条件导出
	 * 
	 * @param response 响应对象
	 * @param paramMap 参数Map
	 */
	@PostMapping(path = "/exportExamResultSub")
	public void exportExamResultSub(HttpServletResponse response, @RequestParam Map<String, Object> paramMap) {
		try {
			List<String> headList = Arrays.asList( "", "", "", "", "");
			List<LinkedHashMap<String, Object>> dataList = examResultSubService.queryExamResultSubForExcel(paramMap);
			ExcelUtils.exportExcel(headList, dataList, "导出信息", response);
		} catch (Exception e) {
			logger.warn(e.toString());
		}
	}

}
