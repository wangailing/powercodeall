package com.powercode.osce.controller;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.powercode.osce.entity.ExamPaperMain;
import com.powercode.osce.service.ExamPaperMainService;
import com.powercode.osce.vo.ExamPaperMainVO;
import com.powercode.component.commons.result.ActionResult;
import com.powercode.component.commons.result.ListResult;
import com.powercode.component.commons.result.ResultBuilder;
import com.powercode.component.commons.utils.ExcelUtils;
import com.powercode.component.commons.validator.InsertValidator;
import com.powercode.component.commons.validator.UpdateValidator;

/**
 * 控制层
 */
@RestController
@RequestMapping("/exampapermain")
public class ExamPaperMainController {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private ExamPaperMainService examPaperMainService;

	/**
	 * 查询分页
	 * 
	 * @param examPaperMainVO 前端参数
	 * @return
	 */
	@GetMapping(path = "/queryExamPaperMain")
	public ListResult<Object> queryExamPaperMain(ExamPaperMainVO examPaperMainVO) {
		Map<String, Object> data = examPaperMainService.queryExamPaperMain(examPaperMainVO.getCurrentPage(), examPaperMainVO.getPageSize(), examPaperMainVO.getSorter());
		return ResultBuilder.buildListSuccess(data);
	}

	/**
	 * 新增
	 * 
	 * @param examPaperMain 对象
	 * @return
	 */
	@PostMapping(path = "/addExamPaperMain")
	public ActionResult addExamPaperMain(@Validated(InsertValidator.class) @RequestBody ExamPaperMain examPaperMain) {
		examPaperMainService.insertExamPaperMain(examPaperMain);
		return ResultBuilder.buildActionSuccess();
	}

	/**
	 * 编辑
	 * 
	 * @param examPaperMain 对象
	 * @return
	 */
	@PutMapping(path = "/updateExamPaperMain")
	public ActionResult updateExamPaperMain(@Validated(UpdateValidator.class) @RequestBody ExamPaperMain examPaperMain) {
		examPaperMainService.updateExamPaperMain(examPaperMain);
		return ResultBuilder.buildActionSuccess();
	}

	/**
	 * 删除
	 * 
	 * @param id ID
	 * @return
	 */
	@PostMapping(path = "/deleteExamPaperMain")
	public ActionResult deleteExamPaperMain(@RequestParam(name = "id", required = true) Long[] id) {
		examPaperMainService.deleteExamPaperMain(id);
		return ResultBuilder.buildActionSuccess();
	}

	/**
	 * 根据查询条件导出
	 * 
	 * @param response 响应对象
	 * @param paramMap 参数Map
	 */
	@PostMapping(path = "/exportExamPaperMain")
	public void exportExamPaperMain(HttpServletResponse response, @RequestParam Map<String, Object> paramMap) {
		try {
			List<String> headList = Arrays.asList( "", "", "", "", "", "");
			List<LinkedHashMap<String, Object>> dataList = examPaperMainService.queryExamPaperMainForExcel(paramMap);
			ExcelUtils.exportExcel(headList, dataList, "导出信息", response);
		} catch (Exception e) {
			logger.warn(e.toString());
		}
	}

}
