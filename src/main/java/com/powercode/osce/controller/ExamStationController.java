package com.powercode.osce.controller;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.powercode.osce.entity.ExamStation;
import com.powercode.osce.service.ExamStationService;
import com.powercode.osce.vo.ExamStationVO;
import com.powercode.component.commons.result.ActionResult;
import com.powercode.component.commons.result.ListResult;
import com.powercode.component.commons.result.ResultBuilder;
import com.powercode.component.commons.utils.ExcelUtils;
import com.powercode.component.commons.validator.InsertValidator;
import com.powercode.component.commons.validator.UpdateValidator;

/**
 * 控制层
 */
@RestController
@RequestMapping("/examstation")
public class ExamStationController {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private ExamStationService examStationService;

	/**
	 * 查询分页
	 * 
	 * @param examStationVO 前端参数
	 * @return
	 */
	@GetMapping(path = "/queryExamStation")
	public ListResult<Object> queryExamStation(ExamStationVO examStationVO) {
		Map<String, Object> data = examStationService.queryExamStation(examStationVO.getCurrentPage(), examStationVO.getPageSize(), examStationVO.getSorter());
		return ResultBuilder.buildListSuccess(data);
	}

	/**
	 * 新增
	 * 
	 * @param examStation 对象
	 * @return
	 */
	@PostMapping(path = "/addExamStation")
	public ActionResult addExamStation(@Validated(InsertValidator.class) @RequestBody ExamStation examStation) {
		examStationService.insertExamStation(examStation);
		return ResultBuilder.buildActionSuccess();
	}

	/**
	 * 编辑
	 * 
	 * @param examStation 对象
	 * @return
	 */
	@PutMapping(path = "/updateExamStation")
	public ActionResult updateExamStation(@Validated(UpdateValidator.class) @RequestBody ExamStation examStation) {
		examStationService.updateExamStation(examStation);
		return ResultBuilder.buildActionSuccess();
	}

	/**
	 * 删除
	 * 
	 * @param id ID
	 * @return
	 */
	@PostMapping(path = "/deleteExamStation")
	public ActionResult deleteExamStation(@RequestParam(name = "id", required = true) Long[] id) {
		examStationService.deleteExamStation(id);
		return ResultBuilder.buildActionSuccess();
	}

	/**
	 * 根据查询条件导出
	 * 
	 * @param response 响应对象
	 * @param paramMap 参数Map
	 */
	@PostMapping(path = "/exportExamStation")
	public void exportExamStation(HttpServletResponse response, @RequestParam Map<String, Object> paramMap) {
		try {
			List<String> headList = Arrays.asList( "", "", "", "", "");
			List<LinkedHashMap<String, Object>> dataList = examStationService.queryExamStationForExcel(paramMap);
			ExcelUtils.exportExcel(headList, dataList, "导出信息", response);
		} catch (Exception e) {
			logger.warn(e.toString());
		}
	}

}
