package com.powercode.osce.controller;

import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.powercode.admin.service.SysDictService;
import com.powercode.osce.entity.ExamProgress;
import com.powercode.osce.service.ExamProgressService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.powercode.osce.entity.ExamTask;
import com.powercode.osce.service.ExamTaskService;
import com.powercode.osce.vo.ExamTaskVO;
import com.powercode.component.commons.result.ActionResult;
import com.powercode.component.commons.result.ListResult;
import com.powercode.component.commons.result.ResultBuilder;
import com.powercode.component.commons.utils.ExcelUtils;
import com.powercode.component.commons.validator.InsertValidator;
import com.powercode.component.commons.validator.UpdateValidator;

/**
 * 控制层
 */
@RestController
@RequestMapping("/examtask")
public class ExamTaskController {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private ExamTaskService examTaskService;
	@Autowired
	private ExamProgressService examProgressService;
	@Autowired
	private SysDictService sysDictService;

	/**
	 * 查询分页
	 * 
	 * @param examTaskVO 前端参数
	 * @return
	 */
	@GetMapping(path = "/queryExamTask")
	public ListResult<Object> queryExamTask(ExamTaskVO examTaskVO) {
		Map<String, Object> data = examTaskService.queryExamTask(examTaskVO.getCurrentPage(), examTaskVO.getPageSize(), examTaskVO.getSorter());
		return ResultBuilder.buildListSuccess(data);
	}

	/**
	 * 新增
	 * 
	 * @param examTask 对象
	 * @return
	 */
	@PostMapping(path = "/addExamTask")
	public ActionResult addExamTask(@Validated(InsertValidator.class) @RequestBody ExamTask examTask , HttpServletRequest request) {
		long id = examTaskService.insertExamTask(examTask);
		ExamProgress ep = new ExamProgress();
		ep.setEntity_id(id);
		ep.setEntity_type(ExamTask.class.getName());
		ep.setUser_id(Long.valueOf(request.getParameter("user_id")));
		ep.setUser_role(Integer.valueOf(request.getParameter("user_role")));
		Map hm = sysDictService.querySysDict(0,1,"osce_examtask_status",null,"ing",null);
		ep.setStep_status(Integer.valueOf((String)hm.get("id"))); // add ing wancheng
		examProgressService.insertExamProgress(ep);
		return ResultBuilder.buildActionSuccess();
	}

	/**
	 * 编辑
	 * 
	 * @param examTask 对象
	 * @return
	 */
	@PutMapping(path = "/updateExamTask")
	public ActionResult updateExamTask(@Validated(UpdateValidator.class) @RequestBody ExamTask examTask) {
		examTaskService.updateExamTask(examTask);
		return ResultBuilder.buildActionSuccess();
	}

	/**
	 * 删除
	 * 
	 * @param id ID
	 * @return
	 */
	@PostMapping(path = "/deleteExamTask")
	public ActionResult deleteExamTask(@RequestParam(name = "id", required = true) Long[] id) {
		examTaskService.deleteExamTask(id);
		return ResultBuilder.buildActionSuccess();
	}

	/**
	 * 根据查询条件导出
	 * 
	 * @param response 响应对象
	 * @param paramMap 参数Map
	 */
	@PostMapping(path = "/exportExamTask")
	public void exportExamTask(HttpServletResponse response, @RequestParam Map<String, Object> paramMap) {
		try {
			List<String> headList = Arrays.asList( "", "", "", "", "", "");
			List<LinkedHashMap<String, Object>> dataList = examTaskService.queryExamTaskForExcel(paramMap);
			ExcelUtils.exportExcel(headList, dataList, "导出信息", response);
		} catch (Exception e) {
			logger.warn(e.toString());
		}
	}

}
