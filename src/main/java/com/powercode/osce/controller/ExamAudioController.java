package com.powercode.osce.controller;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.powercode.osce.entity.ExamAudio;
import com.powercode.osce.service.ExamAudioService;
import com.powercode.osce.vo.ExamAudioVO;
import com.powercode.component.commons.result.ActionResult;
import com.powercode.component.commons.result.ListResult;
import com.powercode.component.commons.result.ResultBuilder;
import com.powercode.component.commons.utils.ExcelUtils;
import com.powercode.component.commons.validator.InsertValidator;
import com.powercode.component.commons.validator.UpdateValidator;

/**
 * 控制层
 */
@RestController
@RequestMapping("/examaudio")
public class ExamAudioController {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private ExamAudioService examAudioService;

	/**
	 * 查询分页
	 * 
	 * @param examAudioVO 前端参数
	 * @return
	 */
	@GetMapping(path = "/queryExamAudio")
	public ListResult<Object> queryExamAudio(ExamAudioVO examAudioVO) {
		Map<String, Object> data = examAudioService.queryExamAudio(examAudioVO.getCurrentPage(), examAudioVO.getPageSize(), examAudioVO.getSorter());
		return ResultBuilder.buildListSuccess(data);
	}

	/**
	 * 新增
	 * 
	 * @param examAudio 对象
	 * @return
	 */
	@PostMapping(path = "/addExamAudio")
	public ActionResult addExamAudio(@Validated(InsertValidator.class) @RequestBody ExamAudio examAudio) {
		examAudioService.insertExamAudio(examAudio);
		return ResultBuilder.buildActionSuccess();
	}

	/**
	 * 编辑
	 * 
	 * @param examAudio 对象
	 * @return
	 */
	@PutMapping(path = "/updateExamAudio")
	public ActionResult updateExamAudio(@Validated(UpdateValidator.class) @RequestBody ExamAudio examAudio) {
		examAudioService.updateExamAudio(examAudio);
		return ResultBuilder.buildActionSuccess();
	}

	/**
	 * 删除
	 * 
	 * @param id ID
	 * @return
	 */
	@PostMapping(path = "/deleteExamAudio")
	public ActionResult deleteExamAudio(@RequestParam(name = "id", required = true) Long[] id) {
		examAudioService.deleteExamAudio(id);
		return ResultBuilder.buildActionSuccess();
	}

	/**
	 * 根据查询条件导出
	 * 
	 * @param response 响应对象
	 * @param paramMap 参数Map
	 */
	@PostMapping(path = "/exportExamAudio")
	public void exportExamAudio(HttpServletResponse response, @RequestParam Map<String, Object> paramMap) {
		try {
			List<String> headList = Arrays.asList( "", "", "", "", "", "", "");
			List<LinkedHashMap<String, Object>> dataList = examAudioService.queryExamAudioForExcel(paramMap);
			ExcelUtils.exportExcel(headList, dataList, "导出信息", response);
		} catch (Exception e) {
			logger.warn(e.toString());
		}
	}

}
